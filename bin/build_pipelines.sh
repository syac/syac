#!/bin/bash
# ------------------------------------------------------------------
# [Author] Nomane Oulali
#          Description
#          $ build_pipelines.sh
# ------------------------------------------------------------------

#set -x

#
if [ -z "$SYAC_HOME" ]; then
    echo "Need to set SYAC_HOME"
    exit 1
fi

#
if [ -z "$SYAC_REPO" ]; then
    echo "Need to set SYAC_REPO"
    exit 1
fi

# Create alias to SYAC executable to be more user friendly in logs
shopt -s expand_aliases # let it, without this line syac alias cannot expand
SYAC_BIN=${SYAC_HOME}/core/syacbatch.py
alias syac="python ${SYAC_BIN}"

for APP_NAME in `ls ${SYAC_REPO} | grep -v syac | grep -v lmg.repo.previous | grep -v base | grep -v .sh`
do
    tput setaf 3
    echo
    echo "--------------------------------------"
    echo "Build pipelines for ${APP_NAME}"
    tput init
    APP_ID=`syac getAppId --appName ${APP_NAME}`
    if [ "$?" -ne 0 ]; then
        tput setaf 1
        echo "WARNING: cannot get appId for this app, check your app.yml"
        tput init
    else
        syac buildPipelines --appId ${APP_ID}
    fi
done
echo


