#!/bin/bash

for i in `ls | grep -v syac | grep -v lmg.repo.previous | grep -v .sh`
do
	tput setaf 2
	echo "Push $i"
	tput init
	cd $i
	git add -A . && git commit -am "incremental" && git push -u origin master
	cd ..
done
