#!/bin/bash

for i in `ls | grep -v syac | grep -v lmg.repo.previous | grep -v .sh`
do
	tput setaf 2
	echo "Pull $i"
	cd $i
	git pull
	cd ..
done
