#!/bin/bash
# ------------------------------------------------------------------
# [Author] Nomane Oulali
#          Description
#          $ deploy_config.sh
# ------------------------------------------------------------------

#set -x

#
if [ -z "$SYAC_HOME" ]; then
    echo "Need to set SYAC_HOME"
    exit 1
fi

#
if [ -z "$SYAC_REPO" ]; then
    echo "Need to set SYAC_REPO"
    exit 1
fi

#
export SYAC_WORKDIR=${SYAC_REPO}/.syac

# Create alias to SYAC executable to be more user friendly in logs
shopt -s expand_aliases # let it, without this line syac alias cannot expand
SYAC_BIN=${SYAC_HOME}/core/syacbatch.py
alias syac="python ${SYAC_BIN}"

# Constants
ROLES_PATH="${SYAC_REPO}/base/config/roles"

# Custom role path / be aware that's ANSIBLE_ROLES_PATH is take in account by Ansible
export ANSIBLE_ROLES_PATH=${ROLES_PATH}

# Parse aguments
while getopts "a:e:" opt; do
    case "$opt" in
      a)  APP_ID=$OPTARG
          ;;
      e)  ENVIRONMENT=$OPTARG
          ;;
      esac
done

[ "$1" = "--" ] && shift

# Set computed context
APP_NAME=`syac getAppName --appId ${APP_ID}`
APP_PATH="${SYAC_REPO}/${APP_NAME}"

if [ ${ENVIRONMENT} = "local" ]
then
    INVENTORY_PATH="${SYAC_WORKDIR}/${ENVIRONMENT}/${APP_ID}.hosts.yml"
else
    # Dynamic Inventory
    INVENTORY_PATH="${SYAC_HOME}/core/inventory/ec2.py"
fi


PLAYBOOK_PATH="${APP_PATH}/config/site.yml"

# Todo Check if all needed files exists

tput setaf 2 # set sharp colour to user see what he try to do
echo
echo "#################################################################################"
echo
echo "  Launch process with context below :"
echo "    - app.id : ${APP_ID}"
echo "    - app.name : ${APP_NAME}"
echo "    - env : ${ENVIRONMENT}"
echo "    - inventory : ${INVENTORY_PATH}"
echo "    - playbook : ${PLAYBOOK_PATH}"
echo
echo "#################################################################################"
echo
tput init # restore standard colour

# Cette commmande est a faire pour chaque app du graphe en avant toute execution CF sinon les slots ne seront pas bons
ANSIBLE_VAR_CONTEXT=`syac getAnsibleVarContext --appId ${APP_ID} --env ${ENVIRONMENT}`

# Start ansible-galaxy from requirements.txt (if exists) to be sure we have all up to date roles
if [ -e ${APP_PATH}/config/requirements.yml ]
then
    echo "Sync roles ..."
    echo
    ansible-galaxy install --roles-path ${ROLES_PATH} -r ${APP_PATH}/config/requirements.yml
fi

# Let's go
echo "Start playbook ..."
echo
# --limit @/Users/nomaneo/project/syac/lmg/app/php/config/main.retry
## --tags php --extra-vars "php_version=5.3"
ansible-playbook $PLAYBOOK_PATH -i ${INVENTORY_PATH} --extra-vars "${ANSIBLE_VAR_CONTEXT}" -f 1

