from setuptools import setup

setup(
    name='syac',
    version='0.0.1',
    packages=[
        'cmdline', 'app', 'utils', 'helper', 'command', 'command.jobs',
        'context', 'inventory'
    ],
    package_dir={
        '': 'core'
    },
    url='www.syac.com',
    license='GPLv3',
    author='Nomane OULALI',
    author_email='nomane@gmail.com',
    install_requires=[
        'boto', 'boto3', 'colorama', 'ansible', 'requests', 'networkx', 'jinja2'
    ],
    description='Cloud Factory',
    long_description='''Syac is a cloud infrastructures factory, aimed at
        helping separate teams in an organization working together to achieve
        deployments of desired and said infrastructures.'''
)
