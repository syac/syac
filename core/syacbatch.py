# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

#   Todo:
#       - exportAppGraph command
#       - 1 dedicated ArgumentParser / command and not an generic one (not clear at all!)
#

import argparse

from command.cmd_builder import CmdBuilder
from context.context import Context

# Parse Args
parser = argparse.ArgumentParser()
parser.add_argument("cmd", help="Which command would you work with ?")
parser.add_argument("--appName", help="Which application name would you work with ?")
parser.add_argument("--appId", help="Which application id name would you work with ?")
parser.add_argument("--env", help="In which environment you want to work ?", default="local")
parser.add_argument("--slot", help="In which slot you want to work ?")
parser.add_argument("--output", help="output file for infra diff report")
parser.add_argument("--chapter", help="In which chapter you want to work ?")
parser.add_argument("--skipIAC", help="Skip IAC ?", default=False)
parser.add_argument("--skipConfig", help="Skip Config ?", default=False)
parser.add_argument("--skipRefresh", help="Skip Refresh ?", default=False)
parser.add_argument('--match', action='append')
args = parser.parse_args()

# Set log level if needed
# logging.basicConfig(level=logging.DEBUG)

# Build context=
context = Context(args)

# Build command
command = CmdBuilder.build_cmd(context, args.cmd, args)

# Run command
command.execute()
