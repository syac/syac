# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

import logging

import utils.file_utils as fileutils


class AppNode:
    def __init__(self, app_id, repo_path, context):

        # Check if conf exists for this app
        fileutils.check_if_app_defined_or_die(context.get_appname(app_id), repo_path)

        # Load conf file
        app_conf_file = fileutils.get_app_conf_path(context.get_appname(app_id), repo_path)

        try:
            params = fileutils.load_yaml(app_conf_file)
        except Exception as e:
            logging.error("Error during loading " + app_id + " configuration file")
            logging.error(e)

        self.appName = context.get_appname(app_id)
        self.appId = params["id"]
        self.factory = params["factory"]
        self.iacStackName = None  # will be compute later once all app graph is builded

        # dependsOf - app id ref
        self.dependsOf = params["depends_of"].split(",")
        depend_of_index = 0
        for dependOf in self.dependsOf:
            self.dependsOf[depend_of_index] = dependOf.strip()
            depend_of_index = depend_of_index + 1

        # extends - app id ref
        self.extends = None
        if "extends" in params:
            self.extends = params["extends"].split(",")
            extends_index = 0
            for extend in self.extends:
                self.extends[extends_index] = extend.strip()
                extends_index = extends_index + 1

        # pipeline behaviour
        self.autogenerate_pipeline = True  # True means pipeline is managed by "syac buildPipelines" command
        if "pipeline" in params:
            if "generate" in params["pipeline"] and str(params["pipeline"]["generate"]).lower() == "manual":
                self.autogenerate_pipeline = False  # Pipeline is managed manually by its author

        # self.iac = IACAppInfo()


class IACAppNode:

    # Possible value
    #        - inherited : heritate from parent
    #        - static : no more instance for given env (e.g. waf)
    #        - dedicated : one new instance  by "create" command
    #        - multitenant : mutualise"""
    FACTORY_TRANSCIENT = "inherited"
    FACTORY_TRANSCIENT = "dedicated"
    FACTORY_TRANSCIENT = "static"
    FACTORY_TRANSCIENT = "multitenant"

    def __init__(self, app_yaml):
        self.factory = ""
