# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

from __future__ import print_function

import os

import networkx as nx
from networkx.drawing.nx_agraph import write_dot

from app.node import AppNode


class AppGraph:

    # todo add work_path pour y generer le .svg
    def __init__(self, app_id, environment, context):
        self.__graph = None
        self.context = context
        self.appId = app_id  # filled on app.yml parsing step
        self.appName = None
        self.basePath = context.homePath
        self.repoPath = context.repoPath
        self.workPath = context.workPath
        self.environment = environment

        # Start Iterate over app dependancies
        self.build_graph()

    def build_graph(self):
        self.__graph = nx.DiGraph()
        self.build_nodes(self.appId)
        self.dump_graph()

    def build_nodes(self, app_id):
        node = AppNode(app_id, self.repoPath, self.context)

        # Add this node to the Graph
        self.__graph.add_node(node.appId, infos=node)

        # Computes dependencies as dependsOf
        for dependOfAppId in node.dependsOf:
            if (dependOfAppId is not None) and (dependOfAppId != "none"):
                self.build_nodes(dependOfAppId)
                self.__graph.add_edge(app_id, dependOfAppId)

    def get_graph(self):
        return self.__graph

    # Dump graph as svg to workdir
    def dump_graph(self):
        # Export graph as file
        dot_filename = self.workPath + os.sep + 'graph_dependencies.dot'
        svg_filename = self.workPath + os.sep + 'graph_dependencies.svg'
        write_dot(self.__graph, dot_filename)
        os.system('dot -Tsvg ' + dot_filename + ' -o ' + svg_filename)

    # Get node infos by app id
    def get_nodeinfos_by_id(self, target_app_id):
        for app_id, app_node in self.__graph.nodes(data=True):
            if app_id == target_app_id:
                return app_node
        return None

    # Get node infos by app name
    def get_nodeinfos_by_name(self, target_app_name):
        for app_id, app_node in self.__graph.nodes(data=True):
            if app_node["infos"].appName == target_app_name:
                return app_node["infos"]
        return None

    # Get node infos by app id
    def get_node_by_id(self, target_app_id):
        return self.__graph[target_app_id]

    # Visit graph to build app dependancies (more to less deeper)
    @staticmethod
    def visit_graph_and_build_app_depencies(dependancy_apps, context):
        graph = context.appGraph.get_graph()
        order = nx.topological_sort(graph, reverse=True)
        for appId in order:
            dependancy_apps.append(appId)

    # Visit graph to build app dependancies (less to more deeper)
    @staticmethod
    def visit_graph_and_build_app_depencies_reverse(dependancy_apps, context):
        graph = context.appGraph.get_graph()
        order = nx.topological_sort(graph, reverse=False)
        for appId in order:
            dependancy_apps.append(appId)

    # return apps which depend of given app_id
    @staticmethod
    def get_dependant_app_from_this(app_id, context):
        # Build graph for given app
        app_graph = AppGraph(app_id, context.environment, context)
        dependancy_apps = []
        order = nx.topological_sort(app_graph.get_graph(), reverse=False)
        for appId in order:
            dependancy_apps.append(appId)
        for index, dependancy_app in enumerate(dependancy_apps):
            if app_id == dependancy_app:
                if len(dependancy_apps) > index:
                    return dependancy_apps[index + 1:]
                else:
                    return []
