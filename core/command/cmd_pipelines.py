# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

import os

import utils.dict_utils as dictutils
import utils.file_utils as fileutils
from command.cmd import Cmd
from command.jobs.job_pipeline import BuildPipelineJob
from helper.resolver import Resolver


class BuildPipelinesCmd(Cmd):
    """
        syac buildPipelines command
        ex: syac buildPipelines -appId myappId
    """

    CMD_NAME = "buildPipelines"

    def __init__(self, context, cmd_args):
        super(BuildPipelinesCmd, self).__init__(context, cmd_args)
        self.create_jobs = []
        self.update_jobs = []
        self.delete_jobs = []
        self.change_jobs = []

    def get_cmd_name(self):
        return self.CMD_NAME

    def on_graph_init(self):
        # Build slot file with next free slot
        Resolver().init_slot_file_from_free(self.context, self.context.appId)

    def validate_args(self, context):
        if not self.cmd_args.appId:
            self.on_parse_args_error(KeyError("Miss 'appId' argument"))

    def usage(self):
        print("syac %s -appId APP_ID", self.get_cmd_name())

    @property
    def is_abstract(self):
        node = self.context.appGraph.get_nodeinfos_by_id(self.context.appId)
        if node['infos'].factory == "abstract":
            return True
        return False

    @property
    def is_autogenerate_enable(self):
        # Should pipelines be build automatically by Syac ?
        node = self.context.appGraph.get_nodeinfos_by_id(self.context.appId)
        return node['infos'].autogenerate_pipeline

    def build_execution_plan(self):

        # Pipeline auto-generate is not applied in following conditions:
        #      1 - Abstract app
        #      2 - pipeline.generate is set to 'manual' in app.yml

        if self.is_abstract:
            print "No pipelines for abstract apps"
            return

        if not self.is_autogenerate_enable:
            print "This app pipelines are manually managed"
            return

        # For 'create' and 'update' traverse from more to less deeper
        dependancy_app_ids_asc = []
        self.context.appGraph.visit_graph_and_build_app_depencies(dependancy_app_ids_asc, self.context)
        for dependancy_app_id in dependancy_app_ids_asc:
            node = self.context.appGraph.get_nodeinfos_by_id(dependancy_app_id)
            if node['infos'].factory != "abstract":
                self.create_jobs.append(BuildPipelineJob(self.context, "create", dependancy_app_id, node))
                self.update_jobs.append(BuildPipelineJob(self.context, "update", dependancy_app_id, node))
                self.change_jobs.append(BuildPipelineJob(self.context, "change", dependancy_app_id, node))

        # For 'delete' traverse from more to less deeper
        dependancy_app_ids_asc = []
        self.context.appGraph.visit_graph_and_build_app_depencies_reverse(dependancy_app_ids_asc, self.context)
        for dependancy_app_id in dependancy_app_ids_asc:
            node = self.context.appGraph.get_nodeinfos_by_id(dependancy_app_id)
            if node['infos'].factory != "abstract":
                self.delete_jobs.append(BuildPipelineJob(self.context, "delete", dependancy_app_id, node))

    # Override base behaviour, each job must return infra, config and refresh data for building pipelines
    def execute(self):
        if self.is_abstract or not self.is_autogenerate_enable:
            return

        # Common params used by all templates
        common_params = {
            "appId": self.context.appId,
            "appName": self.context.get_appname(self.context.appId),
            "base": self.context.base
        }

        # AWS credentials ids section
        aws_credential_ids_def_fragment = ""
        for env in self.context.base["jenkins"]["credentials"]["env"]:
            env_param = {"env": env}
            aws_credential_ids_def_fragment += fileutils.render_template_from_file(
                fileutils.get_fragment_path(self.context, None, "aws.credentials.groovy"),
                dictutils.merge_dicts(common_params, env_param))
            aws_credential_ids_def_fragment += "\n"

        # Checkouts section
        dependancy_app_ids = []
        self.context.appGraph.visit_graph_and_build_app_depencies(dependancy_app_ids, self.context)

        checkout_fragment = self.add_checkout_fragment("base", "base")
        for dependancy_app_id in dependancy_app_ids:
            checkout_fragment += self.add_checkout_fragment(dependancy_app_id,
                                                            self.context.get_appname(dependancy_app_id))

        #
        # Create > Jenkinsfile
        #
        body_fragment = ""
        for job in self.create_jobs:
            body_fragment += job.pipeline["fragment"]
        self.build_pipeline("create", aws_credential_ids_def_fragment, checkout_fragment, common_params, body_fragment)

        #
        # Update > Jenkinsfile
        #
        body_fragment = ""
        for job in self.update_jobs:
            body_fragment += job.pipeline["fragment"]
        self.build_pipeline("update", aws_credential_ids_def_fragment, checkout_fragment, common_params, body_fragment)

        #
        # Delete > Jenkinsfile
        #
        body_fragment = ""
        for job in self.delete_jobs:
            body_fragment += job.pipeline["fragment"]
        self.build_pipeline("delete", aws_credential_ids_def_fragment, checkout_fragment, common_params, body_fragment)

        #
        # Change > Jenkinsfile
        #
        body_fragment = ""
        for job in self.change_jobs:
            body_fragment += job.pipeline["fragment"]
            body_fragment += "\n\n"
        self.build_pipeline("change", aws_credential_ids_def_fragment, checkout_fragment, common_params,
                            body_fragment)

    # Build pipeline for given action
    def build_pipeline(self, action, aws_credential_ids_def_fragment, checkout_fragment, common_params, body_fragment):
        create_params = {
            "aws_credentials_fragment": aws_credential_ids_def_fragment,
            "checkouts_fragment": checkout_fragment,
            "body_fragment": body_fragment
        }
        root_fragment_dir = "change" if action == "change" else None  # 'changset' action has its own root.groovy
        pipeline_content = fileutils.render_template_from_file(
            fileutils.get_fragment_path(self.context, root_fragment_dir, "root.groovy"),
            dictutils.merge_dicts(common_params, create_params))
        # save result in JenkinsFile
        pipeline_dirname = fileutils.get_path_sep().join(self.context.repoPath, self.context.get_appname(self.context.appId),
                                        "pipelines")
        if not os.path.exists(pipeline_dirname):
            os.makedirs(pipeline_dirname)
        pipeline_file = open(fileutils.get_path_sep().join(pipeline_dirname, "Jenkinsfile." + action + ".groovy"), "w")
        pipeline_file.write(pipeline_content.encode('utf-8', 'ignore'))
        pipeline_file.close()
        print(" - " + str(action).capitalize() + " : OK")

    # Append "git checkout" section
    def add_checkout_fragment(self, app_id, app_name):
        return fileutils.render_template_from_file(
            fileutils.get_fragment_path(self.context, None, "checkout.groovy"), {
                "appId": app_id,
                "appName": app_name,
                "base": self.context.base
            }) + "\n"
