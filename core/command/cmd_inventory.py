# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

from command.cmd import Cmd
from command.jobs.job_inventory import GetMatchInventoryJob


class GetMatchInventoryCmd(Cmd):
    """
        syac inventory list of -match MATCH_EXPR
        ex: syac inventory --match "ec2_DBName=espris" --match "ec2_tag_aws_cloudformation_stack_name=clf_tal_waf"
    """

    CMD_NAME = "inventory"

    def get_cmd_name(self):
        return self.CMD_NAME

    def validate_args(self, context):
        if not self.cmd_args.match:
            self.on_parse_args_error(KeyError("Miss 'match' argument"))
        for match_rule in self.cmd_args.match:
            if "=" not in match_rule:
                self.on_parse_args_error(KeyError("Malformated 'match' argument, miss '=': " + match_rule))

    def usage(self):
        print("syac %s -match MATCH_EXPR", self.get_cmd_name())

    def build_execution_plan(self):
        self.jobs.append(GetMatchInventoryJob(self.context, self.context.appId, None, self.cmd_args.match))
