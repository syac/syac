# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

import logging
import os
from abc import ABCMeta, abstractmethod

import helper.banner as banner
from app.graph import AppGraph
from helper.resolver import Resolver


class Cmd:
    """ Command Base Class

        Author: Nomane Oulali"""

    __metaclass__ = ABCMeta

    def __init__(self, context, cmd_args):
        self.jobs = []
        self.cmd_args = cmd_args
        self.cur_job_index = 0
        self.context = context

        # Subclass validation arg hook - raise an Excpetion and print usage if malformatedd
        self.validate_args(cmd_args)

        # Create workdir (if this command need it)
        context.workPath = context.repoPath + os.sep + ".syac" + os.sep
        logging.debug("Create workdir at " + context.workPath)
        if not os.path.exists(context.workPath):
            os.makedirs(context.workPath)

        # If slot is in param init slot file
        if self.cmd_args.slot:
            Resolver().init_slot_file(context, context.appId, self.cmd_args.slot)

        # Some command has not provide appId (e.g. getAppId) cannot build the app graph (and no need)
        if context.appId is not None:
            # Build App graph dependancies based on app.yml
            try:
                context.appGraph = AppGraph(cmd_args.appId, cmd_args.env, context)
                self.on_graph_init()
                # For each app node compute the IAC stack name
                for app_id, app_node in context.appGraph.get_graph().nodes(data=True):
                    if context.environment is not None:
                        app_node['infos'].iac_stack_name = Resolver().get_stackname(context, app_id)
                        logging.debug("App " + app_id + " stack is resolved as " + app_node['infos'].iac_stack_name)

            except Exception as e:
                logging.exception(e)
                raise

    # Hook : Here app graph is ready, forward to observers
    def on_graph_init(self):
        pass

    # Subclass should implement this validation hook
    # if class is has no valid context raise an exception
    @abstractmethod
    def validate_args(self, context):
        pass

    # Build execution plan
    @abstractmethod
    def build_execution_plan(self):
        # Build jobs execution plan
        pass

    # Execute jobs list based on execution plan
    # If an error occurs a rollback is done
    def execute(self):
        for job_index, job in enumerate(self.jobs):
            self.cur_job_index = job_index
            try:
                logging.info("Executing jobs #%s - %s ....", job_index, job.get_name())
                banner.log_frame(job.get_name())
                job.execute(self.context)
            except Exception as e:
                # A jobs failed -> rollback
                logging.exception(e)
                logging.info("\tjobs %s failed, try to rollback", job.get_name())
                for job_to_rollback_index in range(job_index, 0, -1):
                    self.cur_job_index = job_to_rollback_index
                    job_to_rollback = self.jobs[job_to_rollback_index]
                    try:
                        logging.error("\t\tRollback jobs #%s - %s ....",
                                      job_to_rollback_index, job_to_rollback.get_name())
                        job_to_rollback.rollback(self.context)
                    except ValueError as e2:
                        logging.exception(e2)
                        logging.error("\tCannot rollback jobs %s failed, ", job_to_rollback.get_name())
                # Rollback is done, raise exception to upper level
                raise

    # Print usage and raise an exception
    @abstractmethod
    def usage(self):
        pass

    # Error during parsing args
    def on_parse_args_error(self, error):
        self.usage()
        raise error
