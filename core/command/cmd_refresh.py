# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

from command.cmd import Cmd
from command.jobs.job_refresh import GetRefreshVarContextJob


class GetRefreshVarContextCmd(Cmd):
    """
        syac getRefreshVarContext command
        ex: syac getRefreshVarContext -appId myapp -env dev [-slot=02]
    """

    CMD_NAME = "getRefreshVarContext"

    def get_cmd_name(self):
        return self.CMD_NAME

    def validate_args(self, context):
        if not self.cmd_args.appId:
            self.on_parse_args_error(KeyError("Miss 'appId' argument"))
        if not self.cmd_args.env:
            self.on_parse_args_error(KeyError("Miss 'env' argument"))
        if not self.cmd_args.chapter:
            self.on_parse_args_error(KeyError("Miss 'chapter' argument"))

    def usage(self):
        print("syac %s -appId APP_NAME -env ENV [-slot SLOT]", self.get_cmd_name())

    def build_execution_plan(self):
        self.jobs.append(GetRefreshVarContextJob(self.context, self.context.appId, None, self.cmd_args.chapter))
