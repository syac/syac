# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

from command.cmd import Cmd
from command.jobs.job_app import GetAppNameJob, GetAppIdJob
from helper.resolver import Resolver


class GetAppNameCmd(Cmd):
    """
        syac getAppName -app APP_ID
        Return app name from app_id
    """

    CMD_NAME = "getAppName"

    def on_graph_init(self):
        # slot checking is done for each command durong app graph building
        # then create a fake one ... ugly I know ... should be corrected
        # TODO : Fix it !
        Resolver().init_slot_file_from_free(self.context, self.context.appId)

    def get_cmd_name(self):
        return self.CMD_NAME

    def validate_args(self, context):
        if not self.cmd_args.appId:
            self.on_parse_args_error(KeyError("Miss 'appId' argument"))

    def usage(self):
        print("syac %s -appId APP_ID", self.get_cmd_name())

    def build_execution_plan(self):
        self.jobs.append(GetAppNameJob(self.context, self.context.appId, None))


class GetAppIdCmd(Cmd):
    """
        syac getAppId -appName APP_ID
        Return app id from app name
    """

    CMD_NAME = "getAppId"

    def on_graph_init(self):
        # slot checking is done for each command durong app graph building
        # then create a fake one ... ugly I know ... should be corrected
        # TODO : Fix it !
        Resolver().init_slot_file_from_free(self.context, self.context.appId)

    def get_cmd_name(self):
        return self.CMD_NAME

    def validate_args(self, context):
        if not self.cmd_args.appName:
            self.on_parse_args_error(KeyError("Miss 'appName' argument"))

    def usage(self):
        print("syac %s -appName APP_NAME", self.get_cmd_name())

    def build_execution_plan(self):
        job = GetAppIdJob(self.context, self.context.appId, None)
        job.appName = self.cmd_args.appName
        self.jobs.append(job)
