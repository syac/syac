# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

import os.path

from colorama import init, Fore, Style

import helper.banner as banner
from command.cmd import Cmd
from command.jobs.job_cloudformation import BuildInfraJob, UpdateInfraJob, DeleteInfraJob, CompileInfraJob, DiffInfraJob
from helper.resolver import Resolver


class BuildInfraCmd(Cmd):

    """
        syac buildInfra command
        ex: syac buildInfra -appId myappId -env dev
    """

    CMD_NAME = "buildInfra"

    def on_graph_init(self):
        # Build slot file with next free slot
        Resolver().init_slot_file_from_free(self.context, self.context.appId)

    def get_cmd_name(self):
        return self.CMD_NAME

    def validate_args(self, context):
        if not self.cmd_args.appId:
            self.on_parse_args_error(KeyError("Miss 'appId' argument"))
        if not self.cmd_args.env:
            self.on_parse_args_error(KeyError("Miss 'env' argument"))

    def usage(self):
        print("syac %s -appId APP_ID -env ENV".format(self.get_cmd_name()))

    def build_execution_plan(self):
        self.jobs.append(BuildInfraJob(self.context,
                                       self.context.appId,
                                       self.context.appGraph.get_nodeinfos_by_id(self.context.appId)))


class UpdateInfraCmd(Cmd):

    """
        syac updateInfra command
        ex: syac buildInfra -appId myappId -env dev
    """

    CMD_NAME = "updateInfra"

    def get_cmd_name(self):
        return self.CMD_NAME

    def validate_args(self, context):
        if not self.cmd_args.appId:
            self.on_parse_args_error(KeyError("Miss 'appId' argument"))
        if not self.cmd_args.env:
            self.on_parse_args_error(KeyError("Miss 'env' argument"))
        if not self.cmd_args.env:
            self.on_parse_args_error(KeyError("Miss 'slot' argument"))

    def on_graph_init(self):
        # In case of static and no slot argument init slot with value set to "static"
        app_node = self.context.appGraph.get_nodeinfos_by_id(self.context.appId)
        if (app_node['infos'].factory == "static") and not self.cmd_args.slot:
            Resolver().init_slot_file_from_free(self.context, self.context.appId)
        elif not self.cmd_args.slot:
            self.on_parse_args_error(KeyError("Miss 'slot' argument"))

    def usage(self):
        print("syac %s -appId APP_ID -env ENV -slot SLOT".format(self.get_cmd_name()))

    def build_execution_plan(self):
        self.jobs.append(UpdateInfraJob(self.context,
                                        self.context.appId,
                                        self.context.appGraph.get_nodeinfos_by_id(self.context.appId)))


class DeleteInfraCmd(Cmd):

    """
        syac deleteInfra command
        ex: syac deleteInfra -appId myappId -env dev -slot 01
    """

    CMD_NAME = "deleteInfra"

    def get_cmd_name(self):
        return self.CMD_NAME

    def validate_args(self, context):
        if not self.cmd_args.appId:
            self.on_parse_args_error(KeyError("Miss 'appId' argument"))
        if not self.cmd_args.env:
            self.on_parse_args_error(KeyError("Miss 'env' argument"))

    def on_graph_init(self):
        # In case of static and no slot argument init slot with value set to "static"
        app_node = self.context.appGraph.get_nodeinfos_by_id(self.context.appId)
        if (app_node['infos'].factory == "static") and not self.cmd_args.slot:
            Resolver().init_slot_file_from_free(self.context, self.context.appId)

    def usage(self):
        print("syac %s -appId APP_ID -env ENV -slot SLOT".format(self.get_cmd_name()))

    def build_execution_plan(self):
        # Check slot arg, not mandatory for "static", check if it si the case
        # Note: cannot done before
        if not self.cmd_args.slot:
            app_node = self.context.appGraph.get_nodeinfos_by_id(self.context.appId)
            if app_node['infos'].factory != "static":
                self.on_parse_args_error(KeyError("Miss 'slot' argument"))

        # Ok, let's go
        self.jobs.append(DeleteInfraJob(self.context,
                                        self.context.appId,
                                        self.context.appGraph.get_nodeinfos_by_id(self.context.appId)))


class CompileInfraCmd(Cmd):

    """
    syac compileInfra command
    ex: syac compileInfra -appId myappId -env dev
    """

    CMD_NAME = "compileInfra"

    def on_graph_init(self):
        # Build slot file with next free slot
        Resolver().init_slot_file_from_free(self.context, self.context.appId)

    def get_cmd_name(self):
        return self.CMD_NAME

    def validate_args(self, context):
        if not self.cmd_args.appId:
            self.on_parse_args_error(KeyError("Miss 'appId' argument"))
        if not self.cmd_args.env:
            self.on_parse_args_error(KeyError("Miss 'env' argument"))

    def usage(self):
        print("syac %s -appId APP_ID -env ENV".format(self.get_cmd_name()))

    def build_execution_plan(self):
        self.jobs.append(CompileInfraJob(self.context, self.context.appId,
                                         self.context.appGraph.get_nodeinfos_by_id(self.context.appId)))


class DiffInfraCmd(UpdateInfraCmd):
    """
        syac diffInfra command
        ex: syac diffInfra -appId myappId -env dev -slot slot -output myDiffReport.txt
    """

    # TODO : Allow send "Diff report" by mail

    CMD_NAME = "diffInfra"

    def usage(self):
        print("syac %s -appId APP_ID -env ENV -slot SLOT [-output outputFileName]".format(self.get_cmd_name()))

    def build_execution_plan(self):
        self.jobs.append(DiffInfraJob(self.context,
                                      self.context.appId,
                                      self.context.appGraph.get_nodeinfos_by_id(self.context.appId)))

    def execute(self):
        super(DiffInfraCmd, self).execute()
        self.print_report()

    def print_report(self):
        # Init colorama
        init()

        # report content
        report = ""

        # Contain __diffs
        diffs = []

        # Gather diff of each app stackset
        for job_index, job in enumerate(self.jobs):
            if len(job.get_diffs()) != 0:
                diffs += job.get_diffs()

        print(Fore.LIGHTBLUE_EX)
        banner.print_frame("Diff report")

        # How report should looks like
        column_format = "{:37s} {:31s} {:41s} {:41s} {:53s} {:70s}\n"

        # Header contain column names
        header = column_format.format("Action", "StackName", "LogicalId", "Type", "Cause(s)", "CausingEntiti(es)")
        if len(diffs) != 0:
            print header
            for diff in diffs:
                report += column_format.format(diff["Action"], diff["StackName"], diff["LogicalId"], diff["Type"],
                                               ", ".join(diff["Causes"]), ", ".join(diff["CausingEntity"]))
        else:
            report += "No diff"

        # Dump to console
        print report

        # Append report to outputfile if defined
        if hasattr(self.cmd_args, 'output'):
            # write column header name if report file does not exists
            has_header = False
            if os.path.exists(self.cmd_args.output):
                has_header = True
            # content
            file_report = open(self.cmd_args.output, "a")
            if not has_header:
                file_report.write(header)
            file_report.write(report)
            file_report.close()

        print(Style.RESET_ALL)
