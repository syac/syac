# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

from command.cmd_app import GetAppNameCmd, GetAppIdCmd
from command.cmd_config import GetAnsibleVarContextCmd
from command.cmd_infra import BuildInfraCmd, UpdateInfraCmd, DeleteInfraCmd, CompileInfraCmd, DiffInfraCmd
from command.cmd_integration import GetIntegrationVarContextCmd
from command.cmd_inventory import GetMatchInventoryCmd
from command.cmd_localenv import RestartLocalEnvCmd, StartLocalEnvCmd, StopLocalEnvCmd, BuildLocalInventoryCmd
from command.cmd_pipelines import BuildPipelinesCmd
from command.cmd_refresh import GetRefreshVarContextCmd


class CmdBuilder:

    CMDS = {
        # Local env management
        StartLocalEnvCmd.CMD_NAME: StartLocalEnvCmd.__name__,
        StopLocalEnvCmd.CMD_NAME: StopLocalEnvCmd.__name__,
        RestartLocalEnvCmd.CMD_NAME: RestartLocalEnvCmd.__name__,
        BuildLocalInventoryCmd.CMD_NAME: BuildLocalInventoryCmd.__name__,

        # App
        GetAppNameCmd.CMD_NAME: GetAppNameCmd.__name__,
        GetAppIdCmd.CMD_NAME: GetAppIdCmd.__name__,

        # Infra
        BuildInfraCmd.CMD_NAME: BuildInfraCmd.__name__,
        UpdateInfraCmd.CMD_NAME: UpdateInfraCmd.__name__,
        DeleteInfraCmd.CMD_NAME: DeleteInfraCmd.__name__,
        CompileInfraCmd.CMD_NAME: CompileInfraCmd.__name__,
        DiffInfraCmd.CMD_NAME: DiffInfraCmd.__name__,

        # Pipelines
        BuildPipelinesCmd.CMD_NAME: BuildPipelinesCmd.__name__,

        # Config
        GetAnsibleVarContextCmd.CMD_NAME: GetAnsibleVarContextCmd.__name__,

        # Refresh
        GetRefreshVarContextCmd.CMD_NAME: GetRefreshVarContextCmd.__name__,

        # Integration
        GetIntegrationVarContextCmd.CMD_NAME: GetIntegrationVarContextCmd.__name__,

        # Inventory
        GetMatchInventoryCmd.CMD_NAME: GetMatchInventoryCmd.__name__,
    }

    def __init__(self):
        pass

    # return command or raise an error if command does not exists
    @staticmethod
    def build_cmd(context, cmd_name, kwargs):
        if cmd_name not in CmdBuilder.CMDS:
            raise KeyError("Command %s does not exists", cmd_name)
        cmd = globals()[CmdBuilder.CMDS[cmd_name]](context, kwargs)
        cmd.build_execution_plan()
        return cmd
