# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

from command.jobs.job import Job


class GetAppNameJob(Job):
    """syac getAppName -appId esp"""

    def do_execute(self, context):
        app_name = context.get_appname(context.appId)
        print app_name

    def do_rollback(self, context):
        pass


class GetAppIdJob(Job):
    """syac getAppId -appName espris"""

    def do_execute(self, context):
        for app_id, app_name in context.app_id_to_name.iteritems():
            if app_name == self.appName:
                print app_id
                return
        print "Not found"
        exit(23)

    def do_rollback(self, context):
        pass
