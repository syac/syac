# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

import logging
import os
import shutil
from abc import ABCMeta

from command.jobs.job import Job
from inventory.inventory import LocalInventoryMerger


class VagrantJob(Job):
    """ Base abstract class for Vagrant related jobs
        Note : Use Jobs in this file only when you dev locally (especially useful for Ansible)
        Author: Nomane Oulali"""

    __metaclass__ = ABCMeta

    def get_config_path(self):
        return self.context.repoPath + os.sep + self.appId + os.sep + "config"

    def get_vagrantfile_path(self):
        return self.get_config_path() + os.sep + "VagrantFile"

    def is_vagrant_file_exists(self):
        return os.path.exists(self.get_vagrantfile_path())


class StartLocalEnvJob(VagrantJob):
    """Start local environment"""

    def do_execute(self, context):
        # Run "vagrant up" if VagrantFile exists
        if self.is_vagrant_file_exists():
            Job.execute_external_command_and_catch_live_output(
                path=self.get_config_path(),
                program="vagrant",
                args=["up"])
        else:
            logging.info("No Vagrantfile for this app")

    def do_rollback(self, context):
        # No rollback needed
        pass


class StopLocalEnvJob(VagrantJob):
    """Stop local environment jobs"""

    def do_execute(self, context):
        # Run "vagrant destroy" if VagrantFile exists
        if self.is_vagrant_file_exists():
            Job.execute_external_command_and_catch_live_output(
                path=self.get_config_path(),
                program="vagrant",
                args=["destroy --force"])
        else:
            logging.info("No Vagrantfile for this app")

        # Remove .vagrant which is where Vagrant save its cachee
        if self.is_vagrant_cache_exists():
            logging.debug("delete .vagrant cache dir for app %s", self.appId)
            shutil.rmtree(self.get_vagrant_cache_path, ignore_errors=False)

    def do_rollback(self, context):
        # No rollback
        pass

    def is_vagrant_cache_exists(self):
        return os.path.exists(self.get_vagrant_cache_path())

    def get_vagrant_cache_path(self):
        return self.context.repoPath + os.sep + self.appId + os.sep + ".vagrant"


class BuildLocalInventoryJob(VagrantJob):
    """ Build local inventory by merging all dependant apps"""

    def do_execute(self, context):
        LocalInventoryMerger(context, app_id=self.appId).merge()

    def do_rollback(self, context):
        # No rollback
        pass
