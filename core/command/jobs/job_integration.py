# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

import json
import logging

from command.jobs.job import Job
from helper.params import Params


class GetIntegrationVarContextJob(Job):
    """syac getIntegrationVarContext -app espris -env dev -chapter refresh_db.source [-slot 02]"""

    def __init__(self, context, app_id, app_node, chapter):
        super(GetIntegrationVarContextJob, self).__init__(context, app_id, app_node)
        self.chapter = chapter

    def do_execute(self, context):
        params = Params(context, "create", "integration", self.chapter).get_params(self.appId)

        logging.debug("getIntegrationVarContext global result:")
        logging.debug(json.dumps(params, indent=4))
        # print json.dumps(params, indent=4)

        # convert dict to batch args
        result = ""
        for key, value in params[self.chapter].iteritems():
            result += "--" + key + " \"" + value + "\" "

        print result

    def do_rollback(self, context):
        pass
