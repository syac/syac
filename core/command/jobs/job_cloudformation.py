# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

from __future__ import print_function

import json
import logging
import os.path
import shlex
import time
from abc import ABCMeta, abstractmethod
from datetime import datetime
from string import digits

import boto3
from botocore.exceptions import ClientError
from colorama import init, Fore, Style

import helper.params as params_builder
import utils.dict_utils as dictutils
import utils.file_utils as fileutils
from app.graph import AppGraph
from command.jobs.job import Job


class BaseInfraJob(Job):
    """
        Base infratructure as code jobs using CloudFormation

        Useful links:
        -------------
        Create Stack AWS CLI: http://docs.aws.amazon.com/cli/latest/reference/cloudformation/create-stack.html
        CF Status list: http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-cfn-describing-stacks.html

        Author: Nomane Oulali

    """
    __metaclass__ = ABCMeta

    # For local debug
    stub_cloudformation = False

    def __init__(self, context, app_id, app_node):
        super(BaseInfraJob, self).__init__(context, app_id, app_node)
        # Init colorama
        init()
        # list of stack_name and related stack files in the right execution order
        self.stack_execution_plan = []
        # common var context to give to each stack
        self.global_var_context = {}
        # site.yml
        self.code_entry_point = os.path.join(self.context.repoPath, self.context.get_appname(self.appId), "infra",
                                             "site.yml")
        if self.has_infra_code():
            logging.debug(self.appId + " contains infra code")
            self.global_var_context = params_builder.build_global_context(self.context, app_id)
            self.compile(app_id, self.code_entry_point, None)
            self.validate_stack()
        else:
            print("This app has no infra code")

    def do_execute(self, context):
        if self.has_infra_code():
            if not self.stub_cloudformation:
                client = boto3.client('cloudformation')
                # print("Starting " + self.action_label() + " infra for application '"
                # + self.global_var_context["appName"] + "' with this context:")

            self.log_execution_plan()

            for plan in self.stack_execution_plan:
                if self.stub_cloudformation:
                    return
                app_node = context.appGraph.get_nodeinfos_by_id(self.appId)
                print("")
                self.action(plan, app_node, client)

    @abstractmethod
    def action(self, plan, app_node, client):
        pass

    @abstractmethod
    def action_label(self):
        pass

    # Convert params to cloudformation parameters format
    # Note: We have to give only parameters defined in the template that's why we open the template file
    # and looking for awaiting parameters.
    @staticmethod
    def to_cloudformation_parameters(stack_context):
        cf_params = []

        # Cloudformation params
        with open(stack_context["location"]) as stack_file:
            cf_data = json.load(stack_file)

        for param in cf_data["Parameters"]:
            # Standard param(s)
            if param == "Environment":
                if "env" in stack_context["params"]:
                    cf_params.append(
                        BuildInfraJob.to_cloudformation_parameter("Environment", stack_context["params"]["env"]))
                else:
                    print("Miss 'env' parameter")
            else:
                # no need to translate param name
                if param in stack_context["params"]:
                    cf_params.append(
                        BuildInfraJob.to_cloudformation_parameter(param, stack_context["params"][param]))

        print("Forward the following input parameters to Cloud Formation:\n")
        for cf_param in cf_params:
            print(" " + cf_param["ParameterKey"] + "=" + cf_param["ParameterValue"])
        print("")
        return cf_params

    @staticmethod
    def to_cloudformation_parameter(cf_param_name, param_value):
        return {
            'ParameterKey': str(cf_param_name),
            'ParameterValue': param_value,
            'UsePreviousValue': False
        }

    def do_rollback(self, context):
        pass

    # Return true if a site.yml exists
    def has_infra_code(self):
        return os.path.isfile(self.code_entry_point)

    # Compile templates to workdir
    def compile(self, cur_app_id, cur_code_entry_point, dedicated_stack_params):

        if dedicated_stack_params is None:
            dedicated_stack_params = {}

        # open site.yml which contain execution order
        directives = None
        try:
            directives = fileutils.load_yaml(cur_code_entry_point)
        except Exception as e:
            logging.error("Error during loading " + cur_code_entry_point + " configuration file")
            logging.error(e)
            exit(12)

        # Set source infra dir
        infra_source_dir = os.path.join(self.context.repoPath, self.context.get_appname(cur_app_id), "infra")
        logging.debug("infra_source_dir=" + infra_source_dir)

        # Set target (compilation output) infra dir
        infra_output_dir = os.path.join(self.context.workPath, "infra")
        logging.debug("infra_output_dir=" + infra_output_dir)
        if os.path.exists(infra_output_dir) is False:
            os.makedirs(infra_output_dir)

        for directive in directives:
            for key, value in directive.iteritems():
                if key == "include":
                    if isinstance(value, dict):
                        # include with subelement, for yet syac understand "condition" and "append_vars_from"
                        if "condition" in value:
                            # include with condition, check syntax, must contain "==" or "!="
                            condition = str(value["condition"]).strip()
                            if "==" in condition:
                                name, value = condition.split("==")
                                if dedicated_stack_params[name.strip()] == value.strip():
                                    include_file = value["file"]
                                else:
                                    break
                            elif "!=" in condition:
                                name, value = condition.split("==")
                                if dedicated_stack_params[name.strip()] != value.strip():
                                    include_file = value["file"]
                                else:
                                    break
                            else:
                                if dedicated_stack_params[condition]:
                                    include_file = value["file"]
                                else:
                                    break
                        else:
                            include_file = value["file"]

                        # Need to add var from file
                        if "append_vars_from" in value:
                            dedicated_stack_params.update(
                                params_builder.append_vars_from_file(cur_code_entry_point, value["append_vars_from"],
                                                                     dictutils.merge_dicts(self.global_var_context,
                                                                                           dedicated_stack_params)))

                        if "args" in value:
                            args_pair = shlex.split(str(value["args"]))  # Fix issue with space inside quotes
                            for pair in args_pair:
                                key_value = pair.split("=")
                                dedicated_stack_params[key_value[0]] = str(key_value[1]).replace("\"", "")

                    else:
                        # basic include
                        include_file = value

                    # stack_function = include file without extension
                    stack_function = os.path.basename(include_file).split(".")[0]

                    # stack_name = "clf-" + stack_function
                    stack_name = self.global_var_context["base_stack_name"] + "-" + stack_function

                    infra_file = os.path.join(infra_source_dir, include_file)
                    if os.path.isfile(infra_file) is False:
                        raise ValueError(infra_file + " defined in " + cur_code_entry_point + " does not exists")

                    logging.debug("try to compile " + infra_file)
                    compiled = fileutils.render_template_from_file(
                        infra_file, dictutils.merge_dicts(self.global_var_context, dedicated_stack_params))

                    # save compiled template in file
                    compiled_filename = stack_name + ".json"
                    compiled_file = open(os.path.join(infra_output_dir, compiled_filename), "w")
                    compiled_file.write(compiled.encode('utf-8', 'ignore'))
                    compiled_file.close()

                    # update execution plan
                    stack_execution_infos = {
                        # stack_name = clf-{{ env }}-{{ appId }}-{{ stack_function }}
                        # where function is the stack filename minus [.j2].json
                        "name": stack_name,
                        "location": os.path.join(infra_output_dir, compiled_filename),
                        "function": stack_function,
                        "params": self.global_var_context
                    }

                    # append dedicated paremeters to commons
                    stack_execution_infos["params"].update(dedicated_stack_params)
                    self.stack_execution_plan.append(stack_execution_infos)

                elif str(key).startswith("implement:"):
                    # should include parent infra code
                    abstract_app_id = str(key).split(":")[1]
                    logging.debug("This app implement abstract class " + abstract_app_id)
                    if "args" in value:
                        args_pair = shlex.split(str(value["args"]))  # Fix issue with space inside quotes
                        for pair in args_pair:
                            key_value = pair.split("=")
                            dedicated_stack_params[key_value[0]] = str(key_value[1]).replace("\"", "")

                    if "append_vars_from" in value:
                        dedicated_stack_params.update(
                            params_builder.append_vars_from_file(cur_code_entry_point, value["append_vars_from"],
                                                                 dictutils.merge_dicts(self.global_var_context,
                                                                                       dedicated_stack_params)))

                    abstract_class_code_entry_point = os.path.join(self.context.repoPath,
                                                                   self.context.get_appname(abstract_app_id), "infra",
                                                                   "site.yml")
                    # compile abtract class
                    self.compile(abstract_app_id, abstract_class_code_entry_point, dedicated_stack_params)

    # Check if stack is valid
    def validate_stack(self):
        if self.stub_cloudformation:
            return
        client = boto3.client('cloudformation')
        print("")
        print(Fore.LIGHTBLUE_EX + "Validation" + Style.RESET_ALL)
        for plan in self.stack_execution_plan:
            print(plan["name"] + "':", end="")
            stack_file = open(plan["location"], "r")
            template_body = stack_file.read()
            stack_file.close()
            try:
                client.validate_template(TemplateBody=template_body)
            except Exception as e:
                print(Fore.RED + " KO" + Style.RESET_ALL)
                print(e.message)
                exit(4)
            print(Fore.GREEN + " OK" + Style.RESET_ALL)

    # Validate template
    def validate(self):
        pass

    # Return Cloudformation parameters
    def get_params(self):
        pass

    # Log
    def log_execution_plan(self):
        # for index, plan in enumerate(self.stack_execution_plan):
        #    print(Fore.YELLOW + "Stack " + plan["name"] + Style.RESET_ALL)
        #    for key, value in plan.items():
        #        if key != "params" and key != "name":
        #            print("" + key + ": " + value)

        if len(self.stack_execution_plan) > 0:
            print("")
            print(Fore.LIGHTBLUE_EX + "Context" + Style.RESET_ALL)
            print(json.dumps(self.stack_execution_plan[0]["params"], sort_keys=True, indent=4))


class BuildInfraJob(BaseInfraJob):
    def action_label(self):
        return "build"

    def action(self, plan, app_node, client):
        # in case of static we first check is this stack is not already deployed
        if app_node['infos'].factory == "static":
            try:
                client.describe_stacks(StackName=plan["name"])

                # If we are here, that means this stack already exists
                print(Fore.YELLOW + "WARNING: This stack is already deployed, only one instance should exists, "
                                    "then SKIP" + Style.RESET_ALL)
                return
            except Exception as e:
                if "does not exist" not in e.message:
                    # Not "not exist" exception, unknow error exit anyway
                    print(e.message)
                    exit(10)

        # Execution
        stack_file = open(plan["location"], "r")
        template_body = stack_file.read()
        stack_file.close()

        try:
            print(Fore.LIGHTBLUE_EX + "[Start process " + plan["name"] + "]" + Style.RESET_ALL)
            cf_params = self.to_cloudformation_parameters(plan)

            # Create
            response = client.create_stack(
                StackName=plan["name"],
                TemplateBody=template_body,
                Parameters=cf_params,
                TimeoutInMinutes=60,
                OnFailure='ROLLBACK'
            )

            # Poll on stack status
            status = "CREATE_IN_PROGRESS"
            while status == "CREATE_IN_PROGRESS":
                time.sleep(10)  # poll every 10 sec
                response = client.describe_stacks(StackName=plan["name"])
                status = response["Stacks"][0]["StackStatus"]
                print("operation status: " + status)

            # Check status:
            # see http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-cfn-describing-stacks.html
            if status == "CREATE_COMPLETE":
                print(Fore.GREEN + "Stack create SUCCESSFULLY" + Style.RESET_ALL)
            else:
                print(Fore.RED)
                logging.error("Error during stack creation, finish with status: " + status)
                logging.error("Details:")
                logging.error(response)
                print(Style.RESET_ALL)
                exit(6)
        except ClientError as e:
            if e.response["Error"]["Code"] == "AlreadyExistsException":
                logging.info("Stack already exists, continue as if nothing happened")
        except Exception as e:
            print("Failed, msg: " + e.message)
            exit(5)


class UpdateInfraJob(BuildInfraJob):
    def action_label(self):
        return "update"

    def action(self, plan, app_node, client):
        # Execution
        stack_file = open(plan["location"], "r")
        template_body = stack_file.read()
        stack_file.close()

        try:
            print(Fore.LIGHTBLUE_EX + "[Start process " + plan["name"] + "]" + Style.RESET_ALL)
            cf_params = self.to_cloudformation_parameters(plan)

            response = client.update_stack(
                StackName=plan["name"],
                TemplateBody=template_body,
                Parameters=cf_params
            )

            logging.debug(response)

            # Poll on stack status
            status = "UPDATE_IN_PROGRESS"
            while status == "UPDATE_IN_PROGRESS" or status == "UPDATE_COMPLETE_CLEANUP_IN_PROGRESS":
                time.sleep(10)  # poll every 10 sec
                response = client.describe_stacks(StackName=plan["name"])
                status = response["Stacks"][0]["StackStatus"]
                print("operation status: " + status)

            # Check status:
            # see http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-cfn-describing-stacks.html
            if status == "UPDATE_COMPLETE":
                print(Fore.GREEN + "Stack updated SUCCESSFULLY" + Style.RESET_ALL)
            else:
                print(Fore.RED)
                logging.error("Error during stack update, finish with status: " + status)
                logging.error("Details:")
                logging.error(response)
                print(Style.RESET_ALL)
                exit(6)
        except Exception as e:
            # Check if exception is raised because no update are to be performed
            if "No updates are to be performed" in e.message:
                print(Fore.YELLOW + "No update are to be performed for this stack" + Style.RESET_ALL)
            elif "does not exist" in e.message:
                print(Fore.YELLOW + "Warning: This stack does not exists, then create it" + Style.RESET_ALL)
                super(UpdateInfraJob, self).action(plan, app_node, client)
            else:
                print(Fore.RED + "Failed: " + e.message + Style.RESET_ALL)
                exit(5)


class DeleteInfraJob(BuildInfraJob):
    # TODO:     Should delete only application deployed by Syac in order to be secure and consistant.
    # TODO:     That's why we need to save deployed apps in DB (e.g. DynamoDB), then we can delete safely

    def action_label(self):
        return "delete"

    def action(self, plan, app_node, client):
        try:
            print(Fore.LIGHTBLUE_EX + "[Start process " + plan["name"] + "]" + Style.RESET_ALL)

            # If this app is "static" and one of dependant app is running do not proceed
            if app_node['infos'].factory == "static":
                try:
                    # Get all stacks for this environment
                    response = client.describe_stacks()
                    if "Stacks" in response:
                        for stack in response["Stacks"]:
                            clf_stackname = stack["StackName"]
                            if "-" in str(clf_stackname):
                                clf_stackname_splitted = str(clf_stackname).split("-")
                                if len(clf_stackname_splitted) >= 3:
                                    stack_app = str(clf_stackname_splitted[2]).translate(None, digits)  # remove digits
                                    # TODO: If app not found (maybe not deployed by SYAC) not exit in error but ignore
                                    if self.appId in AppGraph.get_dependant_app_from_this(stack_app, self.context):
                                        # If we are here, that means this stack already exists
                                        print(Fore.YELLOW + "WARNING: This static stack is currently in use, "
                                                            "then SKIP" + Style.RESET_ALL)
                                        return
                except Exception as e:
                    print(Fore.RED)
                    logging.error("Error during get stacks to check if this static app is in use")
                    logging.error("Details:")
                    print(e.message)
                    print(Style.RESET_ALL)
                    exit(15)

            # if True:
            #    print("DEBUG")
            #    return

            # do it
            response = client.delete_stack(StackName=plan["name"])
            logging.debug(response)

            # Poll on stack status
            status = "DELETE_IN_PROGRESS"
            while status == "DELETE_IN_PROGRESS":
                time.sleep(10)  # poll every 10 sec
                response = client.describe_stacks(StackName=plan["name"])
                status = response["Stacks"][0]["StackStatus"]
                if "StackStatusReason" in response["Stacks"][0] and "cannot be deleted as it is in use by" \
                        in response["Stacks"][0]["StackStatusReason"]:
                    print(
                        Fore.YELLOW + "This stack cannot be delete because it used by other stack(s)" + Style.RESET_ALL)
                    return
                print("operation status: " + status)

            # Check status:
            # see http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-cfn-describing-stacks.html
            # todo: to delete, this code is unreacheable
            if status == "DELETE_COMPLETE":
                print(Fore.GREEN + "Stack delete SUCCESSFULLY" + Style.RESET_ALL)
            else:
                print(Fore.RED)
                logging.error("Error during stack delete, finish with status: " + status)
                logging.error("Details:")
                logging.error(response)
                print(Style.RESET_ALL)
                exit(6)
        except Exception as e:
            if "does not exist" in e.message:
                print(Fore.GREEN + "Stack delete SUCCESSFULLY" + Style.RESET_ALL)
            else:
                print(Fore.RED + "Failed: " + e.message + Style.RESET_ALL)
                exit(5)


class CompileInfraJob(BaseInfraJob):
    __registry = {}

    def action_label(self):
        return "compile"

    def action(self, plan, app_node, client):
        self.__registry.update({plan["function"]: plan["location"]})

    def do_execute(self, context):
        super(CompileInfraJob, self).do_execute(context)
        print(Fore.LIGHTBLUE_EX + "Output" + Style.RESET_ALL)
        print(json.dumps(self.__registry, sort_keys=True, indent=4))


class DiffInfraJob(BuildInfraJob):
    """
        List diff between deployed stack(s) and latest source code.
        Use this command to secure your deployment by viewing all added, modified or deleted ressources.

        Useful links:

        - http://docs.aws.amazon.com/AWSCloudFormation/latest/APIReference/API_ResourceChangeDetail.html
        - http://docs.aws.amazon.com/AWSCloudFormation/latest/APIReference/API_ResourceChange.html
    """

    __diffs = []

    def action_label(self):
        return "diff"

    def action(self, plan, app_node, client):
        # Execution
        stack_file = open(plan["location"], "r")
        template_body = stack_file.read()
        stack_file.close()

        # ChangeSet name
        changeset_name = 'Change' + datetime.now().strftime('%d%b%H%M')

        try:
            print(Fore.LIGHTBLUE_EX + "[Start process " + plan["name"] + "]" + Style.RESET_ALL)
            cf_params = self.to_cloudformation_parameters(plan)

            response = client.create_change_set(
                StackName=plan["name"],
                TemplateBody=template_body,
                Parameters=cf_params,
                ChangeSetName=changeset_name,
                ChangeSetType='UPDATE'
            )

            logging.debug(response)

            # Poll on stack status
            status = "CREATE_IN_PROGRESS"
            while status == "CREATE_IN_PROGRESS" or status == "CREATE_PENDING":
                time.sleep(3)  # poll every 3 sec
                response = client.describe_change_set(
                    StackName=plan["name"],
                    ChangeSetName=changeset_name
                )
                status = response["Status"]

                # No diff case
                if status == "FAILED" and "The submitted information didn't contain changes" in response[
                    "StatusReason"]:
                    print(Fore.CYAN + "This stack is the same as previous" + Style.RESET_ALL)
                    return

                # Print current Status
                print("Operation status: " + status)

            # Check status
            if status == "CREATE_COMPLETE":
                print(Fore.GREEN + "Stack diff done SUCCESSFULLY" + Style.RESET_ALL)
                # Append diff in buffer
                self.build_report(response)
            else:
                print(Fore.RED)
                logging.error("Error during stack diff, finish with status: " + status)
                logging.error("Details:")
                logging.error(response)
                print(Style.RESET_ALL)
                exit(6)
        except Exception as e:
            # Check if exception is raised because no update are to be performed
            if "does not exist" in e.message:
                print(Fore.YELLOW + "Warning: This stack does not exists" + Style.RESET_ALL)
                exit(7)
            else:
                print(Fore.RED + "Failed: " + e.message + Style.RESET_ALL)
                exit(11)

    def build_report(self, changeset):
        for change in changeset["Changes"]:
            ressource_change = change["ResourceChange"]
            diff = {
                "StackName": changeset["StackName"],
                "LogicalId": ressource_change["LogicalResourceId"],
                "Type": ressource_change["ResourceType"]
            }
            # "Action"
            action = ressource_change["Action"]
            if action == "Modify":
                if "Replacement" in ressource_change:
                    replacement = ressource_change["Replacement"]
                    if replacement == "Conditional":
                        diff["Action"] = "ConditionalDestroyAndReplace"
                    elif replacement == "True":
                        diff["Action"] = "DestroyAndReplace"
                    else:
                        diff["Action"] = "Modify"
                else:
                    diff["Action"] = "Modify"
            else:
                diff["Action"] = action
            # Causing entity and Cause(s) detail(s)
            diff["CausingEntity"] = []
            diff["Causes"] = []
            for detail in ressource_change["Details"]:
                if "CausingEntity" in detail:
                    diff["CausingEntity"].append(detail["CausingEntity"])
                attribute = detail["Target"]["Attribute"]
                if attribute == "Tags":
                    diff["Causes"].append("Tags")
                elif attribute == "Properties":
                    diff["Causes"].append(detail["Target"]["Name"])
                else:
                    diff["Causes"].append(attribute)

            # Add
            self.__diffs.append(diff)

    def get_diffs(self):
        return self.__diffs
