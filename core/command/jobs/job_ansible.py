# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

from command.jobs.job import Job

from helper.resolver import Resolver


class GetAnsibleVarContextJob(Job):
    """syac getAnsibleVarContext -app espris -env dev"""

    def do_execute(self, context):
        # append appid
        var_list = "app=" + context.appId

        # append appname
        var_list += " appName=" + context.get_appname(context.appId)

        # append env
        var_list += " env=" + context.environment

        # append slot
        slot = Resolver().get_slot(context, context.appId)
        if slot == "00":
            raise ValueError("Cannot run Ansible on slot 00, please check that's the infra cloud stack is deployed")

        var_list += " slot=" + slot

        # append list of dependant stack name (including current app)
        for app_id, app_node in self.context.appGraph.get_graph().nodes(data=True):
            var_list += " stack_" + app_id + "=" + \
                        Resolver().get_stackname(context, app_id, True)

        # print result
        print var_list

    def do_rollback(self, context):
        pass
