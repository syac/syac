# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

import json
import logging

import inventory.ec2 as dynamic_inventory_utils
from command.jobs.job import Job


class GetMatchInventoryJob(Job):
    """syac inventory --match "ec2_DBName=espris" --match "ec2_tag_aws_cloudformation_stack_name=clf_tal_waf"""

    def __init__(self, context, app_id, app_node, match_rules):
        super(GetMatchInventoryJob, self).__init__(context, app_id, app_node)
        self.match_rules = {}
        for match_rule in match_rules:
            match_key, match_value = str(match_rule).split("=")
            self.match_rules.update({match_key: match_value})

    def do_execute(self, context):
        result = {}
        dynamic_inventory = dynamic_inventory_utils.Ec2Inventory(True).inventory["_meta"]["hostvars"]
        for hostname, hostdata in dynamic_inventory.iteritems():
            # apply match list
            for match_key, match_value in self.match_rules.iteritems():
                logging.debug("look for match_key=" + match_key + ", match_value=" + match_value)
                if (match_key not in hostdata) or (hostdata[match_key] != match_value):
                    continue
                # Match !
                result.update({hostname: hostdata})
        print json.dumps(result, sort_keys=True, indent=2)

    def do_rollback(self, context):
        pass
