# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

from __future__ import print_function

import logging
import os.path

import utils.dict_utils as dictutils
import utils.file_utils as fileutils
from command.jobs.job import Job


class BuildPipelineJob(Job):
    """
        Compute pipeline data needed by BuildPipelineCmd

        Author: Nomane Oulali
    """

    # For my debug
    __skip_infra = False
    __skip_config = False
    __skip_refresh = True
    __skip_integration = False

    # Conf. name associated to the action
    __action_to_conf = {
        "create": "site.yml",
        "update": "site.yml",
        "delete": "delete.yml",
        "change": "site.yml",
    }

    def __init__(self, context, action, app_id, app_node):
        super(BuildPipelineJob, self).__init__(context, app_id, app_node)

        self.context = context
        self.app_id = app_id
        self.action = action
        self.app_path = os.path.join(self.context.repoPath, self.context.get_appname(app_id))
        self.pipeline = {}  # pipeline content
        self.fragment_base_path = os.path.join(self.context.repoPath, "base", "pipelines")

        # this field will contain app params need by pipelines templates, it will be completed along this class
        self.params = {
            "appId": app_id,
            "appName": context.get_appname(app_id),
            "base": context.base
        }

        # slot arg is applied only for target app in case of update and delete pipeline
        if (action != "create") and (context.appId == app_id) and (app_node['infos'].factory != "static"):
            slot_arg = {"slot_arg": "--slot ${params.slot}"}
        else:
            slot_arg = {"slot_arg": ""}

        # Resolve infra code
        self.pipeline.update({"infra_fragment": ""})
        if not self.__skip_infra and self.has_infra_code:
            logging.debug(app_id + " contain infra code")
            # Note: For infra there is no delete.yml, config is the same as create (site.yml) that's
            # why "create" is provided here.
            if self.get_section_info("infra", "create") is not None:
                self.pipeline.update({"infra_fragment": fileutils.render_template_from_file(
                    fileutils.get_fragment_path(self.context, self.action, "infra.groovy"),
                    dictutils.merge_dicts(self.params, slot_arg))})

        # Resolve config code
        self.pipeline.update({"config_fragment": ""})
        if not self.__skip_config and self.has_config_code:
            logging.debug(app_id + " contain config code")
            if self.get_section_info("config", self.action) is not None:
                self.pipeline.update({"config_fragment": fileutils.render_template_from_file(
                    fileutils.get_fragment_path(self.context, None, "config.groovy"),
                    dictutils.merge_dicts(self.params, slot_arg))})

        # Resolve refresh code (note that refresh step is not present in delete pipeline)
        self.pipeline.update({"refresh_fragment": ""})
        if not self.__skip_refresh and (action != "delete") and self.has_refresh_code:
            logging.debug(app_id + " contain refresh code")
            refresh = self.get_section_info("refresh", self.action)
            if refresh is not None:
                refresh_items_content = self.get_step_content("refresh", refresh["site_path"], slot_arg)
                refresh_body_content = fileutils.render_template_from_file(
                    fileutils.get_fragment_path(self.context, None, "refresh.groovy"),
                    dictutils.merge_dicts(self.params, {"refresh_item_section": refresh_items_content}))
                self.pipeline.update({"refresh_fragment": refresh_body_content})

        # Resolve integration code
        self.pipeline.update({"integration_fragment": ""})
        if not self.__skip_integration and self.has_integration_code:
            logging.debug(app_id + " contain integration code")
            integration = self.get_section_info("integration", self.action)
            if integration is not None:
                integration_items_content = self.get_step_content("integration", integration["site_path"], slot_arg)
                integration_body_content = fileutils.render_template_from_file(
                    fileutils.get_fragment_path(self.context, None, "integration.groovy"),
                    dictutils.merge_dicts(self.params, {"integration_item_section": integration_items_content}))
                self.pipeline.update({"integration_fragment": integration_body_content})

        # Resolve pipeline code
        self.pipeline.update({
            "fragment": fileutils.render_template_from_file(
                fileutils.get_fragment_path(self.context, self.action, "body.groovy"),
                dictutils.merge_dicts(self.params, self.pipeline))
        })

    def get_step_content(self, step, conf_filepath, slot_arg, params=None):
        result = ""
        # read integration/site.yml
        config = fileutils.load_yaml(conf_filepath)
        for section in config:
            if str(section).startswith("implement:"):
                parent_app_id = str(section).split(":")[1]
                parent_conf_filepath = os.path.join(self.context.repoPath, self.context.get_appname(parent_app_id),
                                                    step, self.__action_to_conf[self.action])
                if "args" in config[section]:
                    for arg in config[section]["args"]:
                        if params is None:
                            params = {}
                        params.update(arg)
                    result += self.get_step_content(step, parent_conf_filepath, slot_arg, params)
            else:
                if self.is_condition_ok(config, section, conf_filepath, params):
                    for chapter in config[section]:
                        if chapter != "condition":
                            script_name = config[section][chapter]["file"]
                            script_interpreter = ""
                            if str(script_name).endswith(".sh"):
                                script_interpreter = "bash"
                            elif str(script_name).endswith(".py"):
                                script_interpreter = "python2.7"
                            result += fileutils.render_template_from_file(
                                fileutils.get_fragment_path(self.context, None, step + ".item.groovy"),
                                dictutils.merge_dicts(self.params, slot_arg, {
                                    "section": section,
                                    "script_name": script_name,
                                    "script_interpreter": script_interpreter,
                                    "chapter": section + "." + chapter
                                })) + "\n\n"
        return result

    # Update Jenkins file for CREATE, UPDATE and DELETE
    def do_execute(self, context):
        pass

    def do_rollback(self, context):
        pass

    # Return true if a infra/site.yml exists, check extends app (if exists) as well
    @property
    def has_infra_code(self):
        return (self.get_section_info("infra", "create") is not None) or \
               (self.get_section_info("infra", "delete") is not None)

    # Return true if a config/site.yml exists, check extends app (if exists) as well
    @property
    def has_config_code(self):
        return (self.get_section_info("config", "create") is not None) or \
               (self.get_section_info("config", "delete") is not None)

    # Return true if a refresh/site.yml exists, check extends app (if exists) as well
    @property
    def has_refresh_code(self):
        return (self.get_section_info("refresh", "create") is not None) or \
               (self.get_section_info("refresh", "delete") is not None)

    # Return true if a integration/site.yml exists, check extends app (if exists) as well
    @property
    def has_integration_code(self):
        return (self.get_section_info("integration", "create") is not None) or \
               (self.get_section_info("integration", "delete") is not None)

    def get_section_info(self, section, action):
        path = os.path.join(self.context.repoPath, self.context.get_appname(self.app_id), section,
                            self.__action_to_conf[action])
        if os.path.isfile(path):
            return {"site_app_id": self.app_id, "site_path": path}
        else:
            # check if extends an app, if true check if parent app has code
            app_node = self.context.appGraph.get_nodeinfos_by_id(self.app_id)
            if app_node['infos'].extends is not None:
                for extended_app in app_node['infos'].extends:
                    path = os.path.join(self.context.repoPath, self.context.get_appname(extended_app), section,
                                        self.__action_to_conf[action])
                    if os.path.isfile(path):
                        return {"site_app_id": extended_app, "site_path": path}
        return None

    @staticmethod
    def is_condition_ok(config, section, conf_filepath, params):
        if "condition" in config[section]:
            for condition in config[section]["condition"]:
                if "==" in str(condition):
                    # check if condition is true
                    cond_var_name, cond_var_value = str(condition).split("==")
                    if str(params[cond_var_name]).lower() != str(cond_var_value).lower():
                        return False
                        break
                elif "!=" in str(condition):
                    # check if condition is true
                    cond_var_name, cond_var_value = str(condition).split("!=")
                    if str(params[cond_var_name]).lower() == str(cond_var_value).lower():
                        return False
                        break
                else:
                    raise ValueError("Invalid condition in file " + conf_filepath)
        return True
