# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

import logging
import os
import shutil
import subprocess
import sys

from abc import ABCMeta, abstractmethod
from datetime import datetime


class Job:
    """ Job base (and abstract) class

        Author: Nomane Oulali"""

    __metaclass__ = ABCMeta

    # Job state
    WAITING = 0
    IN_PROGRESS = 1
    FAILED = 2
    DONE = 3
    CANCELED = 4

    def __init__(self, context, app_id, app_node):
        self.context = context  # Global context
        self.appContext = app_node  # As Job manage one App, we have here a dedicated context
        self.appId = app_id
        self.state = self.WAITING
        self.startTime = -1

    # Execute jobs
    def execute(self, context):
        # logging.debug("Execute jobs " + self.get_name())
        if self.state is not self.WAITING:
            raise Exception('Error during executings jobs ' + self.get_name() + ' -> is not in WAITING state')
        self.startTime = datetime.now().time()
        self.state = self.IN_PROGRESS
        self.do_execute(context)

    # Subclass execution implementation
    @abstractmethod
    def do_execute(self, context):
        pass

    # Rollback this jobs
    def rollback(self, context):
        logging.info("Rollback jobs %s", self.get_name())
        self.do_rollback(context)

    # Subclass rollback implementation
    @abstractmethod
    def do_rollback(self, context):
        pass

    # return jobs name
    def get_name(self):
        return self.__class__.__name__
        # return os.path.basename(self.dirPath)

    # Get work dir for this job
    @staticmethod
    def create_job_dir_path(context, job_index, app_id, job_type):
        job_index_formatted = str(job_index).zfill(2)
        job_file_name = job_index_formatted + "_" + app_id + "_" + job_type;
        job_dir_path = context.workPath + os.sep + "jobs" + os.sep + job_file_name
        logging.debug("Job #" + job_index_formatted + " -> " + job_dir_path)
        if os.path.exists(job_dir_path):
            # strange there is an old jobs dir here, log and delete it
            logging.debug("Job directory " + job_dir_path + " already exists, delete it !")
            shutil.rmtree(job_dir_path, ignore_errors=False)
        os.makedirs(job_dir_path)
        return job_dir_path

    @staticmethod
    def execute_external_command_and_catch_live_output(path, program, args):
        # with open('test.log', 'w') as f:
        process = subprocess.Popen(program + " " + " ".join(args), cwd=path, shell=True, stdout=subprocess.PIPE)
        for line in iter(process.stdout.readline, ''):
            sys.stdout.write(line)
            # f.write(line)
