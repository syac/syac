# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

import logging

from command.cmd import Cmd
from command.jobs.job_vagrant import StartLocalEnvJob, StopLocalEnvJob, BuildLocalInventoryJob
from helper.resolver import Resolver


class StartLocalEnvCmd(Cmd):
    """startLocalEnv command"""

    CMD_NAME = "startLocalEnv"

    def get_cmd_name(self):
        return self.CMD_NAME

    def on_graph_init(self):
        # Build slot file with next free slot
        Resolver().init_slot_file_from_free(self.context, self.context.appId)

    # [Implement] Subclass should implement this validation hook
    def validate_args(self, context):
        if not self.cmd_args.appId:
            self.on_parse_args_error(KeyError("Miss 'appId' argument"))

    def usage(self):
        print("syac %s -appId APP_NAME", self.get_cmd_name())

    def build_execution_plan(self):
        self.jobs = []

        for app_id, app_node in self.context.appGraph.get_graph().nodes(data=True):
            self.jobs.append(StartLocalEnvJob(self.context, app_id, app_node))


class StopLocalEnvCmd(Cmd):
    """stopLocalEnv command"""

    CMD_NAME = "stopLocalEnv"

    def get_cmd_name(self):
        return self.CMD_NAME

    def on_graph_init(self):
        # Build slot file with next free slot
        Resolver().init_slot_file_from_free(self.context, self.context.appId)

    # [Implement] Subclass should implement this validation hook
    def validate_args(self, cmd_args):
        if not cmd_args.appId:
            self.on_parse_args_error(KeyError("Miss 'appId' argument"))

    def usage(self):
        print("syac %s -appId APP_NAME", self.get_cmd_name())

    def build_execution_plan(self):
        self.jobs = []

        for app_id, app_node in self.context.appGraph.get_graph().nodes(data=True):
            self.jobs.append(StopLocalEnvJob(self.context, app_id, app_node))


class RestartLocalEnvCmd(Cmd):
    """stopLocalEnv command"""

    CMD_NAME = "restartLocalEnv"

    def on_graph_init(self):
        # Build slot file with next free slot
        Resolver().init_slot_file_from_free(self.context, self.context.appId)

    # [Implement] Subclass should implement this validation hook
    def validate_args(self, context):
        if "appId" not in self.cmd_args:
            self.on_parse_args_error(KeyError("Miss 'appId' argument"))

    def usage(self):
        print("syac %s -appId APP_NAME", self.get_cmd_name())

    def build_execution_plan(self):
        job_index = 1

        for app_id, app_node in self.context.appGraph.get_graph().nodes(data=True):
            self.jobs.append(StopLocalEnvJob(self.context, app_id, app_node))
            job_index += 1
            self.jobs.append(StartLocalEnvJob(self.context, app_id, app_node))
            job_index += 1


class BuildLocalInventoryCmd(Cmd):
    """buildLocalInventory command"""

    CMD_NAME = "buildLocalInventory"

    def get_cmd_name(self):
        return self.CMD_NAME

    def on_graph_init(self):
        # Build slot file with next free slot
        Resolver().init_slot_file_from_free(self.context, self.context.appId)

    # [Implement] Subclass should implement this validation hook
    def validate_args(self, context):
        # Todo
        pass

    def usage(self):
        print("syac %s --app APP_NAME --env ENV", self.get_cmd_name())

    def build_execution_plan(self):
        self.jobs = []
        job_index = 1

        for app_id, app_node in self.context.appGraph.get_graph().nodes(data=True):
            # Asbtract class should not have inventory
            if app_node["infos"].factory != 'abstract':
                self.jobs.append(BuildLocalInventoryJob(self.context, app_id, app_node))
                job_index += 1
            else:
                logging.debug("App " + app_id + " is abstract then no inventory will be generated")
