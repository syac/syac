import logging
import os

import utils.file_utils as fileutils


class GlobalConfig:

    # Conf file
    CONF_FILENAME = "app.yml"  # ex syac.yml

    def __init__(self, app_id, environment):
        self.app_id = app_id
        self.environment = environment

    #
    # Fill Context object with global configuration
    #
    def load(self, context):
        context.appId = self.app_id
        context.environment = self.environment
        try:
            context.homePath = os.environ.get('SYAC_HOME')
        except Exception as e:
            logging.error("Please define SYAC_HOME environment variable")
            raise
        try:
            context.repoPath = os.environ.get('SYAC_REPO')
        except Exception as e:
            logging.error("Please define SYAC_REPO environment variable")
            raise

        # Workpath stuff will be filled in Cmd base class
        context.workPath = None

        # Load base/site.yml which contain syac global config
        try:
            params = fileutils.load_yaml(context.repoPath + os.sep + "base" + os.sep + self.CONF_FILENAME)
        except Exception as e:
            logging.error("Error during loading configuration file")
            logging.error(e)
            raise
        try:
            context.base = params

            # context.apiURL = params['api']
            # context.domain = params['domain']

            # -> Environment
            # context.envPolicies = {}
            # for env in {"dev", "integration", "preprod", "prod"}:
            #    for envParams in params["environments"]:
            #        if env in envParams:
            #            context.envPolicies[env] = EnvironmenPolicy(env, envParams[env])
            #            break
        except Exception as e:
            logging.exception(e)
            raise


class EnvironmenPolicy:
    """Environment Policy"""
    def __init__(self, env_name, params):
        self.envName = env_name
        for param in params:
            if "slots" in param:
                self.slots = param['slots']
            if "pattern" in param:
                self.pattern = param['pattern']
            if "vpc" in param:
                self.vpc = param['vpc']
            if "zoneA" in param:
                self.zoneA = param['zoneA']
            if "zoneB" in param:
                self.zoneB = param['zoneB']
            if "monitoring" in param:
                self.monitoring = param['monitoring']
