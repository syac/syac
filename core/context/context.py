import logging
import os.path

import utils.file_utils as fileutils
from config import GlobalConfig


class Context:

    # This class is a singleton and should be accessible from everywhere
    ___instance = None

    # Allow to find appname from appid
    app_id_to_name = {}

    @classmethod
    def get_instance(cls):
        if cls.___instance is None:
            raise ValueError("Context should be configured at init.")
        return cls.___instance

    def __init__(self, args):
        self.___instance = self
        self.appId = None
        self.environment = None
        self.base = None  # contain base configuration from SYAC_REPO/base/site.yml
        self.homePath = None
        self.repoPath = None
        self.workPath = None
        self.appGraph = None

        # Compute skipping flags
        self.skipIAC = args.skipIAC
        self.skipConfig = args.skipConfig
        self.skipRefresh = args.skipRefresh

        # Build Config from base/site.yml
        GlobalConfig(args.appId, args.env).load(self)

        # Build app_id_to_name dict
        # We need it to find app_id from app_name
        if self.repoPath is not None:
            for app_name in os.listdir(self.repoPath):
                if app_name not in ["base", ".syac", "syac"]:
                    if os.path.isdir(os.path.join(self.repoPath, app_name)):
                        # Check if cur app has an app.yml
                        app_conf_file = fileutils.get_app_conf_path(app_name, self.repoPath)
                        if os.path.isfile(app_conf_file):
                            params = fileutils.load_yaml(app_conf_file)
                            self.app_id_to_name[params["id"]] = app_name
                        else:
                            logging.debug("Application " + app_name + " has no conf under " + app_conf_file)

    # Return app_name from app_id
    def get_appname(self, app_id):
        if app_id in self.app_id_to_name:
            return self.app_id_to_name[app_id]
        else:
            raise ValueError("App with id " + app_id + " does not exists")
