import json
import logging
import os

import yaml
from jinja2 import Template

import inventory.ec2 as dynamic_inventory_utils
import utils.dict_utils as dictutils
import utils.file_utils as fileutils
from resolver import Resolver


# Build context with commons vars
def build_global_context(context, app_id):
    global_var_context = {
        "base_stack_name": Resolver().get_stackname(context, app_id, False),
        "appId": app_id,
        "appName": context.get_appname(app_id),
        "slot": Resolver().get_slot(context, app_id),
        "env": context.environment}

    # Build a list with all app dependencies
    dependancy_apps = []
    context.appGraph.visit_graph_and_build_app_depencies(dependancy_apps, context)
    for dependancy_app in dependancy_apps:
        global_var_context["stack_" + dependancy_app] = Resolver().get_stackname(context, dependancy_app, False)

    logging.debug("varContext for " + app_id + ":")
    logging.debug(global_var_context)

    return global_var_context


# Append params from input file to stack params
def append_vars_from_file(source_file_path, directive, var_context_to_merge):
    params = None

    if directive is not None:
        for var_file_name in directive:
            var_file_abs_path = os.path.join(os.path.dirname(source_file_path), var_file_name)
            # translate jinja expr if exists
            var_file_abs_path = Template(var_file_abs_path).render(dictutils.merge_dicts(var_context_to_merge, params))
            try:
                compiled_template = fileutils.render_template_from_file(var_file_abs_path,
                                                                        dictutils.merge_dicts(var_context_to_merge,
                                                                                              params))
                var_file = yaml.load(compiled_template)
            except Exception as e:
                logging.error("Error during loading " + var_file_abs_path + " var file included in " + source_file_path)
                logging.error(e)
                exit(14)
            if params is None:
                params = var_file
            else:
                params.update(var_file)

    return params


#
# V2 - Migrate code above to class below
#


class Params:
    action_to_conf_file = {
        "create": "site.yml",
        "delete": "delete.yml"
    }

    def __init__(self, context, action, section, chapter):
        self.context = context
        self.base_params = None
        self.app_params = None
        self.conf_filename = self.action_to_conf_file[action]
        self.section_params = None
        self.section = section
        self.chapter = chapter

    # Section could be: infra, config, refresh or integration
    def get_params(self, app_id):
        base_params = self.get_base_params()
        app_params = self.get_app_params(app_id)
        section_params = self.get_section_params(app_id, dictutils.merge_dicts(base_params, app_params))
        return dictutils.merge_dicts(base_params, app_params, section_params)

    def get_base_params(self):
        return {"base": self.context.base}

    def get_app_params(self, app_id):
        app_params = {
            "homepath": self.context.homePath,
            "workdir": self.context.workPath,
            "base_stack_name": Resolver().get_stackname(self.context, app_id, False),
            "appId": app_id,
            "appName": self.context.get_appname(app_id),
            "slot": Resolver().get_slot(self.context, app_id),
            "env": self.context.environment}
        # append from app.yml
        app_config_filename = os.path.join(self.context.repoPath,
                                           self.context.get_appname(app_id),
                                           "app.yml")
        app_params.update(fileutils.load_yaml(app_config_filename))

        # append dependant stack names
        dependancy_apps = []
        self.context.appGraph.visit_graph_and_build_app_depencies(dependancy_apps, self.context)
        for dependancy_app in dependancy_apps:
            app_params["stack_" + dependancy_app] = Resolver().get_stackname(self.context, dependancy_app, False)

        #
        return app_params

    def get_section_params(self, app_id, cumulated_params={}):
        section_config_filename = os.path.join(self.context.repoPath, self.context.get_appname(app_id), self.section,
                                               self.conf_filename)
        if not os.path.isfile(section_config_filename):
            return cumulated_params

        config = fileutils.load_yaml(section_config_filename)
        chapter_config = None

        # Look for target chapter
        chapter_deep_level = str(self.chapter).count(".")
        if chapter_deep_level > 3:
            print("Error: Cannot computes chapter params deep than 3 levels, check your configuration at %s",
                  section_config_filename)
            exit(28)
        try:
            chapter_as_path = str(self.chapter).split(".")
            if len(chapter_as_path) == 0:
                chapter_config = config
            elif len(chapter_as_path) == 1:
                chapter_config = config[chapter_as_path[0]]
            elif len(chapter_as_path) == 2:
                chapter_config = config[chapter_as_path[0]][chapter_as_path[1]]
            elif len(chapter_as_path) == 3:
                chapter_config = config[chapter_as_path[0]][chapter_as_path[1]][chapter_as_path[2]]
        except Exception as e:
            logging.debug(e)
            logging.debug("Chapter not found, look for implement: section")
            # This chapter is not present in this conf, look for implement:
            for directive in config:
                if str(directive).startswith("implement:"):
                    parent_app_id = str(directive).split(":")[1]
                    if "args" in config[directive]:
                        for arg in config[directive]["args"]:
                            cumulated_params.update(arg)
                        # parse params from implemented absract app
                        return self.get_section_params(parent_app_id, cumulated_params)
            print("Error: chapter " + self.chapter + " not found, check your config at " + section_config_filename)
            exit(29)

        # Check if there is condition(s) in this level, skip if is the case and is false
        if "condition" in chapter_config:
            condition_ok = True
            for condition in chapter_config["condition"]:
                if "==" in str(condition):
                    # check if condition is true
                    cond_var_name, cond_var_value = str(condition).split("==")
                    if str(cumulated_params[cond_var_name]).lower() != str(cond_var_value).lower():
                        condition_ok = False
                        break
                elif "!=" in str(condition):
                    # check if condition is true
                    cond_var_name, cond_var_value = str(condition).split("!=")
                    if str(cumulated_params[cond_var_name]).lower() == str(cond_var_value).lower():
                        condition_ok = False
                        break
                else:
                    raise ValueError("Invalid condition in file " + section_config_filename)
            if not condition_ok:
                return cumulated_params

        # Append vars from append_vars_from
        if "append_vars_from" in chapter_config:
            cumulated_params.update(
                self.append_vars(
                    section_config_filename,
                    chapter_config["append_vars_from"],
                    cumulated_params))

        section_args = {}

        if "args" in chapter_config:
            for arg_key, arg_value in chapter_config["args"].iteritems():
                arg_value = Template(arg_value).render(cumulated_params)
                section_args.update({arg_key: arg_value})

        cumulated_params.update({self.chapter: section_args})

        return cumulated_params

    # Append params from input file, ec2.py or simple var definition to stack params
    def append_vars(self, source_file_path, directive, var_context_to_merge):
        params = None

        if directive is not None:
            for var_file_name in directive:
                if isinstance(var_file_name, str):
                    var_file_abs_path = os.path.join(os.path.dirname(source_file_path), var_file_name)
                    # translate jinja expr if exists
                    var_file_abs_path = Template(var_file_abs_path).render(
                        dictutils.merge_dicts(var_context_to_merge, params))
                    try:
                        compiled_template = fileutils.render_template_from_file(var_file_abs_path,
                                                                                dictutils.merge_dicts(
                                                                                    var_context_to_merge, params))
                        var_file = yaml.load(compiled_template)
                    except Exception as e:
                        logging.error(
                            "Error during loading " + var_file_abs_path + " var file included in " + source_file_path)
                        logging.error(e)
                        exit(14)
                    if params is None:
                        params = var_file
                    else:
                        dictutils.merge_dicts_recursively(params, var_file)
                elif isinstance(var_file_name, dict):
                    if "ec2.py" in var_file_name:
                        ec2py = var_file_name["ec2.py"]
                        ec2py_result = self.get_ec2_py_param(ec2py, dictutils.merge_dicts(var_context_to_merge, params))
                        if ec2py_result is None:
                            print(
                                "Error: query to ec2.py failed (host not found) from file " + source_file_path + " for chapter " + self.chapter + " and directive:")
                            print json.dumps(var_file_name, sort_keys=True, indent=2)
                            exit(30)
                        params.update({ec2py["var_name"]: ec2py_result})
                else:
                    print("Error: Invalid config file at " + source_file_path)
        #
        return params

    @staticmethod
    def get_ec2_py_param(query, var_context):
        dynamic_inventory = dynamic_inventory_utils.Ec2Inventory(True).inventory["_meta"]["hostvars"]
        for hostname, hostdata in dynamic_inventory.iteritems():
            # apply match list
            for match_key, match_value in query["match"].iteritems():
                match_value = Template(match_value).render(var_context)
                logging.debug("look for match_key=" + match_key + ", match_value=" + match_value)
                if (match_key not in hostdata) or (hostdata[match_key] != match_value):
                    continue
                # Match !
                return hostdata[query["get"]]
        # No match
        return None
