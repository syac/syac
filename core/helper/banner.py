import logging


def log_frame(name, style_character='*'):
    """Frame the name with the style_character."""

    frame_line = style_character * (len(name) + 4)
    logging.debug(frame_line)
    logging.debug('{0} {1} {0}'.format(style_character, name))
    logging.debug(frame_line)


def print_frame(name, style_character='*'):
    """Frame the name with the style_character."""

    frame_line = style_character * (len(name) + 4)
    print(frame_line)
    print('{0} {1} {0}'.format(style_character, name))
    print(frame_line)
