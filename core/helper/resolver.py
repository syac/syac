# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Written by Nomane OULALI

import logging
import os

import boto3

import utils.file_utils as fileutils


class Resolver:

    """
    Resolve slot, hostname or stackname
    Author : Nomane Oulali
    """

    #
    # __init__
    def __init__(self):
        pass

    # Init slot file with next free slot
    def init_slot_file_from_free(self, context, app_id):
        free_slot = self.get_free_slot(context, app_id)
        self.init_slot_file(context, app_id, free_slot)

    # Init slot file with input slot index
    def init_slot_file(self, context, app_id, slot):
        slot_config = {}
        if os.path.isfile(self.get_slot_filename(context)):
            slot_config = fileutils.load_yaml(self.get_slot_filename(context))
        slot_config[app_id] = str(slot).zfill(2)
        fileutils.save_yaml(self.get_slot_filename(context), slot_config)

    @staticmethod
    def get_free_slot(context, app_id):
        app_node = context.appGraph.get_nodeinfos_by_id(app_id)
        if app_node['infos'].factory != "static":
            if context.environment == "local":
                # in local mode there take slot 1 as default
                return "01"
            final_app_id = app_id
            if app_node['infos'].factory == "abstract":
                # abstract then not final, try to finc the implem. app
                final_app_id = Resolver.get_implementation_app_from_abstract(context, app_id)
            node = context.appGraph.get_nodeinfos_by_id(final_app_id)
            base_stackname = Resolver.get_stackbasename(context, final_app_id)
            ec2 = boto3.resource('ec2')
            slot = 1
            while True:
                instances = ec2.instances.filter(
                    Filters=[{'Name': 'tag:Application', 'Values': [node["infos"].appName]},
                             # Not take in account terminated instances
                             {'Name': 'instance-state-name',
                              'Values': ["pending", "running", "shutting-down", "stopping", "stopped"]},
                             {'Name': 'tag:aws:cloudformation:stack-name',
                              'Values': [base_stackname + str(slot).zfill(2) + "*"]}])
                if (sum(1 for _ in instances) == 0) or (slot == 10):
                    # this slot is free since instances collection size is equal to zero
                    break
                else:
                    slot = slot + 1

            # return free slot in 2 digits format
            return str(slot).zfill(2)

        else:
            return "static"

    def is_slot_file_exists(self, context):
        return os.path.isfile(self.get_slot_filename(context))

    @staticmethod
    def get_slot_filename(context):
        return os.path.join(context.workPath, "slots.yml")

    #
    # get_slot
    # Return slot for this session from ./syac/slot file
    def get_slot(self, context, app_id, force_empty_for_static=False):
        if not self.is_slot_file_exists(context):
            print("Error: slot file does not exists")
            exit(25)

        app_node = context.appGraph.get_nodeinfos_by_id(app_id)

        slot_config = fileutils.load_yaml(self.get_slot_filename(context))

        if app_node['infos'].factory == "abstract":
            extend_app_id = self.get_implementation_app_from_abstract(context, app_id)
            if extend_app_id in slot_config:
                self.init_slot_file(context, app_id, slot_config[extend_app_id])
                slot_config = fileutils.load_yaml(self.get_slot_filename(context))
            else:
                print("Error: slot for %s which implement abstract app %s file does not exists", app_id, extend_app_id)
                exit(26)

        if app_id not in slot_config:
            if app_node['infos'].factory == "static":
                self.init_slot_file_from_free(context, app_id)
                slot_config = fileutils.load_yaml(self.get_slot_filename(context))
            else:
                print("Error: slot for app %s file does not exists", app_id)
                exit(27)

        slot = slot_config[app_id]

        if slot == "static":
            if force_empty_for_static:
                return ""
            else:
                return "01"
        else:
            return slot

    #
    # get_stackname
    # Return App infra stackname :
    # A CF stack follow this format: clf-{{ env }}-{{ app }}{{ slot }}
    #   - {{ app }} : if given app is 'abstract' return implementation name
    #   - {{ slot }} : if app is 'static' return "01"
    def get_stackname(self, context, app_id, inventory_compatible=False):
        stack_name = Resolver.get_stackbasename(context, app_id)
        # force_empty_for_static=True because in case of static no slot in stack name
        stack_name += self.get_slot(context, app_id, force_empty_for_static=True)
        if inventory_compatible:
            # Ansible inventory does not support "-" then replace it by "_" as ec2.py do
            stack_name = stack_name.replace("-", "_")
        return stack_name

    # Private / Return base stack name just before slot
    @staticmethod
    def get_stackbasename(context, app_id):
        base_stack_name = "clf-" + context.environment + "-"

        # if app is abstract find its implementation
        app_node = context.appGraph.get_nodeinfos_by_id(app_id)

        if len(app_node) == 0:
            raise ValueError('App with id ' + app_id + ' does not exists')

        # {{ app }}
        if app_node['infos'].factory == "abstract":
            # Find which app implement this interface
            extend_app_id = Resolver.get_implementation_app_from_abstract(context, app_id)
            if extend_app_id is not None:
                base_stack_name += str(extend_app_id).replace("_",
                                                              "")  # Need this substition to be compatible with CF constraint
            else:
                # Implementation app not found raise an exception
                raise ValueError('No app implement abstract class ' + app_id)
        else:
            base_stack_name += app_id.replace("_", "")  # Need this substition to be compatible with CF constraint

        return base_stack_name

    @staticmethod
    def get_implementation_app_from_abstract(context, abstract_app_id):
        for cur_app_id, cur_app_node in context.appGraph.get_graph().nodes(data=True):
            if cur_app_node['infos'].extends is not None:
                for cur_extend_app_id in cur_app_node['infos'].extends:
                    if cur_extend_app_id == abstract_app_id:
                        return cur_app_id
        # No implementation found return abstract class as default with warning
        logging.debug(abstract_app_id + " abstract class has no implementation")
        return abstract_app_id
