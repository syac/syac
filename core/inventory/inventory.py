import logging
import os
import os.path
from sets import Set

from jinja2 import Template


class LocalInventoryMerger:
    def __init__(self, context, app_id):
        self.context = context
        self.appId = app_id
        self.basePath = context.workPath + os.sep + app_id
        self.hostFileName = self.basePath + ".hosts.yml"
        pass

    def merge(self):

        # Build a Set with all app dependencies
        dependancy_apps = []
        self.context.appGraph.visit_graph_and_build_app_depencies(dependancy_apps, self.context)

        # Log dependancies
        log = "App " + self.appId + " depends of"
        for dependancy_app in dependancy_apps:
            log += " " + dependancy_app
        logging.debug(log)

        # Merge inventories
        inventory = {}
        for dependancy_app in dependancy_apps:
            self.merge_inventory(self.context,
                                 dependancy_app,
                                 self.context.appGraph.get_nodeinfos_by_id(dependancy_app),
                                 inventory)

        # Save merged inventory under workdir
        template_inventory_file = open(self.hostFileName, "w")

        for group, hosts in sorted(inventory.items()):
            template_inventory_file.write(group + "\n")
            for host in hosts:
                template_inventory_file.write(host + "\n")
            template_inventory_file.write("\n")
        template_inventory_file.close()

    @staticmethod
    def merge_inventory(context, app_id, app_node, inventory):
        # Check if the inventory file exists for this app
        inventory_file_path = context.repoPath + os.sep + app_id \
                              + os.sep + "config" + os.sep + "inventories" + os.sep + "local" + os.sep + "hosts.yml"
        if os.path.exists(inventory_file_path) is False:
            # No inventory to merge for this app
            logging.debug("App " + app_id + " does not have inventory")
            return

        # Template context
        template_context = {
            'stack_name': str(app_node['infos'].iac_stack_name).replace("-", "_")
        }

        # Inventory exists, start merging
        inventory_file = open(inventory_file_path)
        cur_group = None
        for line in iter(inventory_file):
            # Resolve Jinja expression before
            line = Template(line).render(template_context)
            line = line.strip()
            # If start by [ means host group
            if line.startswith("["):
                if line in inventory:
                    cur_group = inventory[line]
                else:
                    cur_group = Set()
                    inventory[line] = cur_group
            # not blank line and no comment
            elif len(line) is not 0 and not line.startswith("#"):
                cur_group.add(line)

