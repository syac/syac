import logging
import os
import os.path
from os import environ

import yaml
from jinja2 import Environment, FileSystemLoader, StrictUndefined


# Load YAML
def load_yaml(path):
    logging.debug("Load yaml from %s" % path)
    with open(path, 'r') as stream:
        return yaml.load(stream)


# Save YAML
def save_yaml(path, data):
    logging.debug("Save yaml to %s" % path)
    with open(path, 'w') as stream:
        yaml.dump(data, stream, default_flow_style=False)


# Return path to app.yml
def get_app_conf_path(app_name, repo_path):
    return repo_path + os.sep + app_name + os.sep + "app.yml"


# Check if app.yml exists
def check_if_app_defined_or_die(app_name, repo_path):
    app_conf_file = get_app_conf_path(app_name, repo_path)
    if not os.path.exists(app_conf_file):
        raise Exception('App ' + app_name + " cannot be found at " + app_conf_file)


# Resolve Jinja template file
def render_template_from_file(tpl_path, context):
    path, filename = os.path.split(tpl_path)
    # StrictUndefined is set on to raise an exception in case of undefined variable
    return Environment(loader=FileSystemLoader(path or './'), undefined=StrictUndefined) \
        .get_template(filename).render(context)


# Return pipeline fragment path
def get_fragment_path(context, action, frament_file_name):
    if action is not None:
        return os.path.join(context.repoPath, "base", "pipelines", action, frament_file_name)
    else:
        return os.path.join(context.repoPath, "base", "pipelines", frament_file_name)


# Return path separator and take in account if you are under Cygwin on Windows
def get_path_sep():
    if ("TERM" in environ) and (str(environ['TERM']).lower() == "cygwin"):
        return "/"
    else:
        return os.path
