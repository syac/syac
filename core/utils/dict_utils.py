import collections


def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    """
    result = {}
    if dict_args is not None:
        for dictionary in dict_args:
            if dictionary is not None:
                result.update(dictionary)
        return result
    return None


# TODO : Replace all calls from merge_dicts to merge_dicts_recursively, to enhance merging (deeply merge) nut need nonreg tests
def merge_dicts_recursively(dct, merge_dct):
    """ From https://gist.github.com/angstwad/bf22d1822c38a92ec0a9
    Recursive dict merge. Inspired by :meth:``dict.update()``, instead of
    updating only top-level keys, dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys. The ``merge_dct`` is merged into
    ``dct``.
    :param dct: dict onto which the merge is executed
    :param merge_dct: dct merged into dct
    :return: None
    """
    for k, v in merge_dct.iteritems():
        if (k in dct and isinstance(dct[k], dict)
            and isinstance(merge_dct[k], collections.Mapping)):
            merge_dicts_recursively(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]
