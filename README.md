# syac

A cloud factory.

# Usage

Le but du jeu:

``  
syac --cmd [listApp|newApp|updateApp|deleteApp] --env $DEV --id $APPID [--slot $SLOTID] [--skip refresh, config, iac]
```

## liste les applications dans l'environnement de dev
syac --cmd listApp --env dev
GET api.syac.com/app/dev/list

## liste les applications espris dans l'environnement de dev
syac --cmd listApp --env dev --id espris
GET api.syac.com/app/espris/dev/list

## Installe une nouvelle application espris en dev de type devXX.espris.fr ou XX est le prochain slot libre
syac --cmd newApp --env dev --id espris
POST api.syac.com/app/espris/new

## Installe une nouvelle application espris en dev sur le slot dev01.espris.fr si celui n'est pas libre retourne une erreur
syac --cmd newApp --env dev --id espris --slot 1
POST api.syac.com/app/espris/new/1

## Mise à jour (on peux skipper iac et/ou config et/ou refresh) en skippant le iac
syac --cmd updateApp --env dev --id espris --slot 1 --skip iac
POST api.syac.com/app/espris/dev/update/1

## Raffraichit espris en dev01 depuis la prod en skippant les chapitre db et assets (voir refresh.yml)
syac --cmd refreshApp --env dev --id --slot 01 --skip db,assets
POST api.syac.com/app/espris/dev/1/refresh

## Supprime dev01.espris.fr
syac --cmd deleteApp --env dev --id espris --slot 1
DELETE api.syac.com/app/dev/espris/1

update

# Installation

Install dependancies

```
python setup.py install_requires
python setup.py install
```

Execute unittest

```
python -m unittest
```

# Contributing

The best way to author modifications in our opinion is to fork the software
annd apply them.

If you want these modifications to enter the mainline you will have to send the
maintainer team your signed DCO (which can be found at the root of your source
code copy under the file name `DCO.txt`).
