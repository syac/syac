import argparse
import json
import logging
import os
import subprocess

import requests
from colorama import init, Fore, Style

'''
    File name: monitoring.py
    Author: Nomane Oulali
    Python Version: 2.7
    Description: Ajout / Suppression d'un groupe de serveur(s) appartenant a une stack Cloudformation
    Example: python2.7 /Users/nomaneo/project/syac/syac/ext/monitoring.py --action add --endpoint http://ilsupdca01/nagios_ws/clapi/index.php --hosttemplate hosttemplate --hostgroup hostgroup --pollername pollername --location location --servicenow_type servicenow_type --type type --application application --stack clf-tal-eel01-web
'''

# Check if SYAC_HOME is defined
syac_home = os.environ.get('SYAC_HOME')
if syac_home is None:
    print("Error: SYAC_HOME is not defined, please set this environment variable before using this script.")
    exit(2)

# Init colorama
init()

# syac bin
syac = os.path.join(syac_home, "core", "syacbatch.py")

# Parse Args
parser = argparse.ArgumentParser()
parser.add_argument("--action", help="Ajout ou decommissionner une mise sous supervision", required=True)
parser.add_argument("--endpoint", help="Nom ou IP du serveur Centreon", required=True)
parser.add_argument("--hosttemplate",
                    help="Template(s) a associer au serveur. S'il y en a plusieurs, utiliser '|' comme separateur sans espace. (ex : taoreshst_tp_24x7|taoreshst_linux_nrpe_base)",
                    required=True)
parser.add_argument("--hostgroup",
                    help="Hostgroup(s) a associer au serveur. S'il y en a plusieurs, utiliser '|' comme separateur sans espace. (ex : PRODUCTION|ATOS_AUBERVILLIERS)",
                    required=True)
parser.add_argument("--pollername", help="Nom du poller ou doit etre ajoute le nouveau server (Central)",
                    default="Central")
parser.add_argument("--location", help="Localisation du serveur.", default="AMAZON")
parser.add_argument("--servicenow_type", help="Type du serveur dans service Now", required=True)
parser.add_argument("--type", help="Type de serveur Centreon", required=True)
parser.add_argument("--application", help="Application hoste par le serveur", required=True)
parser.add_argument("--stack",
                    help="Non de la stack cloudformation auquel auquel extraire les EC2 auquel l'action doit s'appliquer",
                    required=True)
args = parser.parse_args()

if str(args.action).lower() not in ["add", "remove"]:
    print("Invalid action, shoud be 'add' or 'remove'.")
    exit(4)

# Print args
print("Input args:")
print(args)

# Get host list belong to the stack
syac_get_inventory_call = ["/usr/local/bin/python2.7", syac, "inventory", "--match",
                           "ec2_tag_aws_cloudformation_stack_name=" + args.stack]
logging.debug("Get host from inventory ...")
logging.debug(syac_get_inventory_call)
syac_process = subprocess.Popen(syac_get_inventory_call, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
get_inventory_out, get_inventory_err = syac_process.communicate()
returncode = syac_process.returncode
if returncode != 0:
    print(Fore.RED)
    print("Error during getting hosts belong to stack " + args.stack)
    if get_inventory_out is not None:
        print("Stdout: " + get_inventory_out)
    if get_inventory_err is not None:
        print("Stderr: " + get_inventory_err)
    print(Style.RESET_ALL)
    exit(3)

# List EC2(s) hostvars from the given stack
logging.debug("Inventory result:")
logging.debug(get_inventory_out)

inventory = json.loads(get_inventory_out)

error_occur = False

for host, host_data in inventory.iteritems():

    host_ip = host_data["ec2_private_ip_address"]
    host_name = str(host_data["ec2_tag_Hostname"]).upper()

    print(Fore.YELLOW)
    print("-------------------------------------------------")
    print(" " + host_name)
    print("-------------------------------------------------")
    print(Style.RESET_ALL)

    # Check if action should be applied for this server by calling "check" API
    check_payload = {
        "action": "3",
        "host": host_name,
        "poller": args.pollername
    }
    print("Call check API " + args.endpoint + " with:")
    print (json.dumps(check_payload, sort_keys=True, indent=4))
    r = requests.get(args.endpoint, params=check_payload)

    if r.status_code != 200:
        print(Fore.RED + "Error: 'check' action call failed with status: " + str(
            r.status_code) + " reason: " + r.reason + "\n" + Style.RESET_ALL)
        error_occur = True
        continue

    print("Check API response => " + r.text)

    # Parse response which follow this format:
    # [CODE_RETOUR_WEBSERVICE]:HOST STATE :[NOMBRE_HOST]
    # ex: 0:HOST STATE:0
    check_status, check_host_state, check_host_number = r.text.split(":")

    if int(check_status) != 0:
        print(
        Fore.RED + "Error: 'check' action call status failed with status: " + check_status + "\n" + Style.RESET_ALL)
        error_occur = True
        continue

    if args.action == "add":
        # Add

        if check_host_number > 0:
            print("This host is already monitored, no need to call 'add' action")
            continue

        payload = {
            "host": host_name,
            "ip": host_ip,
            "action": "1",
            "poller": args.pollername,
            "hstpl": args.hosttemplate,
            "hg": args.hostgroup,
            "loc": args.location,
            "typ": args.type,
            "sntyp": args.servicenow_type,
            "app": args.application
        }
    else:
        # Remove

        if check_host_number == 0:
            print("This host is not monitored, no need to call 'delete' action")
            continue

        payload = {
            "host": host_name,
            "action": "2",
            "poller": args.pollername
        }

    print("Call " + args.endpoint + " with:")
    print (json.dumps(payload, sort_keys=True, indent=4))
    r = requests.get(args.endpoint, params=payload)

    if r.status_code != 200:
        print(Fore.RED)
        print("Error: this call failed with status: " + str(r.status_code) + " reason: " + r.reason + "\n")
        print(Style.RESET_ALL)
        error_occur = True
        continue

    # TODO : Scanner r.text pour verifier que c'est OK

    print(Fore.GREEN)
    print("Result: OK")
    print(Style.RESET_ALL)

if error_occur:
    exit(5)
